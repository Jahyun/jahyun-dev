imprt { createStore }
from 'redux';

// Action 정의
const increment = () => {
    return (type: 'INCREMENT');
}
const decrement = () => {
    return (type: 'DECREMENT');
}

// Reducer 함수 정의
// 1st parameter (inital state), 2nd parameter (action)
const counterReducer = (state = 0, action) => {
    switch (action.type) {
        case 'INCREMENT':
            return state + 1;
        case 'DECREMENT':
            return state - 1;
    }
}

export default counterReducer;

let store = createStore(counter); // counter 결과를 store에 저장

//Display it in the console
store.subscribe(() => console.log(store.getState()));

// Dispatch
store.dispatch(increment());
store.dispatch(decrement());



export default counter;

switch0, actiaction.typ) {
    case 'INCREMENT':
        return state + 1;
    case 'DECREMENT':
        return state - 1;
};

let store = createStore()






export default Say;



const loggedReducer = (state = false, action) => {
    switch (action.type) {
        case 'SIGN_IN':
            return !state;
    }
}
export default loggedReducer;