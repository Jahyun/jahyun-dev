// imr : import React
import React from 'react';

// imrimrc : import React/Componentc
import React, { Component } from 'react';

//imrs : import React /useState
import React, { useState } from 'react';

/* ------------------------------------------- */
// sfc : Stateless Function Component (Arrow function)
const example = () => {
    return (  );
}
export default example;

// ffc : function component
function example() {
    return (  );
}
export default example;

/* ------------------------------------------- */
//cc: class component
class example extends Component {
    state = {  } 
    render() { 
        return ();
    }
}
export default example;

//ccc: class component with constructor
class example extends Component {
    constructor(props) {
        super(props);
    }
    state = {  }
    render() { 
        return (  );
    }
}
export default example;

//ren
render() {
    return (
         
    );
}


// uef : useEffect Hook
useEffect(() => {
    
}, []);
/* ------------------------------------------- */

// ss: setState
this.setState({ Example:  });

//ssf : function setState
this.setState(prevState => {
    return { ABC: prevState.ABC };
});

//usf: Declare a new state variable using State Hook
const [ABC, setABC] = useState();


/* ------------------------------------------- */

//cp: Context Provider
export const example = React.createContext();

const exampleProvider = (props) => {
    // Initial Provider State
    const state = {};

    // Reducer Function
    const  = (state, action) => {
        
    };

    return (
        <example.Provider value={{state: state, 
        }}>
{props.children}
 
 </example.Provider>
    );
}
export default exampleProvider;
