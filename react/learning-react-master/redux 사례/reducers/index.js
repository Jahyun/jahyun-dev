import counterReducer from './counter';
import loggedReducer from '../isLogged';
import { combineReducer } from 'redux';

const allReducers = combineReducer({
    counter: counterReducer,
    isLogged: loggedReducer
})