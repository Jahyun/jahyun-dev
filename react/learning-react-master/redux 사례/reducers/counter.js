const counterReducer = (state = 0, action) => {
    switch (action.type) {
        case 'INCREMENT':
            return state + 1;
        case 'DECREMENT':
            return {
                ...state,   // spread함수를 사용해서 불변성을 유지
                state: state - 1;
            }
        default:
            return state;
    }
}

export default counterReducer;