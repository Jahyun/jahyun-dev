import React from 'react';

const IterationSample() => {

    const [names, setNames] = useState([ 
        {id:1, text:'눈사람'}, 
        {id:2,text:'얼음'}
    ]);
    const [inputText, setInputText] = useState('');
    const [nextId, setNextId] = useState('5');
    
    const onChange = e => setInputText(e.target.value);

    // concat 내장함수를 활용하여 배열에서 데이터 추가
    const onClick =() => {
        const nextNames = names.concat ({
            id: nextId,
            text: inputText
        })
        setNextId (nextId + 1);
        setNames(nextNames);
        setInputText('');
    };

    // filter 내장함수를 활용하여 배열에서 데이터 삭제
    const onRemove = id => {
        const nextNames = names.filter(name => name.id !== id);
        setNames(nextNames);
    };

    const namesList = names.map(name => (
        <li key={name.id} onDoubleClick={()=> onRemove(name.id)}>
            {name.text}
        </li>
    ));
    
    const namesList = names.map(name => <li key={name.id}>{name.text}</li>);

    return (
        <>
            <input value={inputText} onChange={onChange} />
            <button onClick={onClick}> 추가 </button>
            <ul>{namesList}</ul>
        </>
    )
}

export default IterationSample;