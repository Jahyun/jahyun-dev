import React, { useState } from "react";
import { ethers } from "ethers";

function WalletCard() {
  const [errorMessage, setErrorMessage] = useState(null);
  const [defaultAccount, setDefaultAccount] = useState(null);
  const [userBalance, setUserBalance] = useState(null);
  const [connButtonText, setConnButtonText] = useState("Connect Wallet");

  const connectWalletHandler = () => {
    if (window.ethereum) {
      window.ethereum
        .request({ method: "eth_requestAccounts" })
        .then((result) => {
          accountChangeHandler(result[0]);
        });
    } else {
      setErrorMessage("Install MetaMask");
    }
  };
 
  const accountChangeHandler = (newAccount) => {
    setDefaultAccount(newAccount);
    getUserBalance(newAccount.toString());
  };

  const getUserBalance = (address) => {
    window.ethereum
      .request({ method: "eth_getBalance", params: [address, "lastest"] })
      .then((balanceResult) => {
        setUserBalance(ethers.utils.formatEther(balanceResult));
      }); 

  const chainChangedHandler = () => {
    window.location.reload();
  };

  window.ethereum.on("accountsChanged", accountChangeHandler);

  window.ethereum.on("chainChanged", chainChangedHandler);

  return (
    <div calssName="walletCard" style={{ backgroundColor: "#ffffff" }}>
      <h1> WalletCard.js 파일 코드 활용 결과...</h1>
      <h1> Connect to Metamask wallet</h1>
      <h4> {"Connection to MetaMask using window.ethereum methods"}</h4>
      <button onClick={connectWalletHandler}>{connButtonText}</button>
      <p></p>
      <div className="accountDisplay">
        <h3> Address: {defaultAccount}</h3>
      </div>
      <div calssName="balanceDisplay">
        <h3> Balance: {userBalance}</h3>
      </div>
    </div>
  );
}}

export default WalletCard;
