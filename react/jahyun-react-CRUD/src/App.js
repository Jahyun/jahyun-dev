import {Button, Grid, Icon, Divider} from "semantic-ui-react";
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>

        <Divider />

        <Grid Columns={1}>

        <button  className="ui button red" > Follow </button>
          <Button icon labelPosition='left'>
            <Icon name='pause' />
            Pause
          </Button>
          <Button icon labelPosition='right'>
            Next
            <Icon name='right arrow' />
          </Button>
        </Grid>
      </header>
    </div>
  );
}

export default App;
