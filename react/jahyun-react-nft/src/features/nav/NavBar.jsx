import React from "react";
import { Menu, Button, Container } from "semantic-ui-react";

function NavBar({ setSelectedMenu }) {
  return (
    <Menu inverted fixed="top">
      <Container>
        <Menu.Item header>
          <img
            src={require("../../assets/logo.png")}
            alt="logo"
            size="large"
            style={{ marginRight: 15 }}
          />
          NFT Marketplace
        </Menu.Item>
        <Menu.Item onClick={() => setSelectedMenu("Home")} name="Home" />
        <Menu.Item onClick={() => setSelectedMenu("Gallery")} name="Gallery" />
        {/* <Menu.Item>
            <Button onClick={()=>setSelectedMenu("Gallery")} basic inverted content='Gallery'></Button>
          </Menu.Item> */}

        <Menu.Item onClick={() => setSelectedMenu("Create")} name="Create" />
        <Menu.Item onClick={() => setSelectedMenu("Trade")} name="Trade" />
        <Menu.Item
          onClick={() => setSelectedMenu("TestPage1")}
          name="TestPage1"
        />
        <Menu.Item
          onClick={() => setSelectedMenu("CRUD Module")}
          name="CRUD Module"
        />

        <Menu.Item
          onClick={() => setSelectedMenu("MetaConnectPage")}
          name="MetaConnectPage"
        />

        <Menu.Item position="right">
          <Button primary onClick={() => setSelectedMenu("ConnectWallet")}>
            Connect Wallet
          </Button>
        </Menu.Item>
      </Container>
    </Menu>
  );
}

export default NavBar;
