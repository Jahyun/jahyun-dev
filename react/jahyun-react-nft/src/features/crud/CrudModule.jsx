import React, { useState } from "react";
import { Grid, Header, Button, Container } from "semantic-ui-react";
import styles from "./CrudModule.module.css";
import EventDashboard from "./events/EventDashboard";

function CrudModule() {
  const [formOpen, setFormOpen] = useState(false);
  const [selectedEvent, setSelectedEvent] = useState(null);

  // 선택한 event의 내용이 있는 form을 보여준다  (View 버튼 선택시)
  function handleSelectEvent(event) {
    setSelectedEvent(event);
    setFormOpen(true);
  }

  // 비어있는 form을 보여준다  (Create Event 버튼 선택시)
  function handleCreateFormOpen() {
    setSelectedEvent(null);
    setFormOpen(true);
  }

  return (
    <Container>
      <div className={styles.header}>
        <Grid >
          <Grid.Row>
            <Grid.Column width={6}>
              <Header as="h1">CRUD Module</Header>
              <p> Create, Read, Update and Delete.</p>
            </Grid.Column>
            <Grid.Column width={4}>
              <Button primary onClick={() => handleCreateFormOpen()}>
                Create Event
              </Button>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
      <div className={styles.list}>
        <EventDashboard
          formOpen={formOpen}
          setFormOpen={setFormOpen}
          selectEvent={handleSelectEvent}
          selectedEvent={selectedEvent}
        />
      </div>
    </Container>
  );
}

export default CrudModule;
