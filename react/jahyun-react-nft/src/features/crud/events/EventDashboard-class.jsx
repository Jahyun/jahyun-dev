import React from "react";
import { useState } from "react";
import { Grid } from "semantic-ui-react";
import EventForm from "../eventForm/EventForm";
import EventList from "./EventList";
import { SampleData } from "../../../app/api/SampleData";

class EventDashboard extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            events: SampleData
         }

         this.handleCreateEvent = this.handleCreateEvent.bind(this);
         this.handleUpdateEvent = this.handleUpdateEvent.bind(this);
    }
    
    handleCreateEvent(event) {
        this.setState(({events}) => ({
            events: [...events, event]
        }));
        this.props.setFormOpen(false);
      }

      handleUpdateEvent(updatedEvent){
        this.setState(({events}) => ({
            events: events.map((evt) => (evt.id === updatedEvent.id ? updatedEvent : evt))
        }));
        this.props.selectEvent(null);
        this.props.setFormOpen(false);
      }

      handleDeleteEvent = (eventId) => {
        this.setState(({events}) => ({
            setEvents(events.filter((evt) => evt.id !== eventId))
        }));
      }

    render() { 

        const { selectEvent, setFormOpen, setEvents, selectedEvent, formOpen} = this.props;


        return ( 
            <Grid>
      <Grid.Column width={10}>
        {/* state로 정의한 events를 {events}로 가져온다 */}
        <EventList
          events={events}
          selectEvent={selectEvent}
          deleteEvent={handleDeleteEvent}
        />
      </Grid.Column>
      <Grid.Column width={6}>
        {formOpen && (
          <EventForm
            setFormOpen={setFormOpen} // 입력 폼 Open하는 함수 제공
            createEvent={handleCreateEvent} // 입력한 내용으로 새로운 Event 추가하는 함수를 제공
            selectedEvent={selectedEvent} // 선택된 event를 제공
            updateEvent={handleUpdateEvent}
            key={selectedEvent ? selectedEvent.id : null}
            // Key; event 선택되어 있으면 그 event의 id를 key에 넣고, event 선택안된 create event이면 null을 넣는다.
          />
        )}
      </Grid.Column>
    </Grid>
         );
    }
}
 
export default EventDashboard;


// function EventDashboard({ formOpen, setFormOpen, selectEvent, selectedEvent }) {
//     const [events, setEvents] = useState(SampleData);
  
//     function handleCreateEvent(event) {
//       setEvents([...events, event]);
//       // events 기존 데이터 가져오고, 새로운 event 데이터를 추가한다는 의미
//       //**** EventForm에서는 { }로 감쌌는데, 여기는 : [] 등이 없는 간단한 변수만 있어서 그냥 []로 감싸는 것 같다. */
//     }
  
//     function handleUpdateEvent(updatedEvent) {
//       // setEvents로 업데이트한 데이터를 추가함
//       // id가 같은 것을 찾아서, updatedEvent 내용을 저장하고, 같은 id가 앖으면 evt를 그대로 둔다.
//       setEvents(
//         events.map((evt) => (evt.id === updatedEvent.id ? updatedEvent : evt))
//       );
//       selectEvent(null);
//     }
  
//     function handleDeleteEvent(eventId) {
//       // eventId 외의 모든 id의 event를 filter해서 모두 event에 다시 저장한다.
//       setEvents(events.filter((evt) => evt.id !== eventId));
//     }
  
//     return (
//       <Grid>
//         <Grid.Column width={10}>
//           {/* state로 정의한 events를 {events}로 가져온다 */}
//           <EventList
//             events={events}
//             selectEvent={selectEvent}
//             deleteEvent={handleDeleteEvent}
//           />
//         </Grid.Column>
//         <Grid.Column width={6}>
//           {formOpen && (
//             <EventForm
//               setFormOpen={setFormOpen} // 입력 폼 Open하는 함수 제공
//               createEvent={handleCreateEvent} // 입력한 내용으로 새로운 Event 추가하는 함수를 제공
//               selectedEvent={selectedEvent} // 선택된 event를 제공
//               updateEvent={handleUpdateEvent}
//               key={selectedEvent ? selectedEvent.id : null}
//               // Key; event 선택되어 있으면 그 event의 id를 key에 넣고, event 선택안된 create event이면 null을 넣는다.
//             />
//           )}
//         </Grid.Column>
//       </Grid>
//     );
//   }
  
//   export default EventDashboard;
