import React from "react";
import EventListItem from "./EventListItem";

function EventList({ events, selectEvent, deleteEvent }) {
  return (
    <>
      {/* events={SampleData} ; SampleData 내의 모든 data 항목을 map 으로 모두 가져온다 */}
      {events.map((eventTemp) => (
        <EventListItem
          event={eventTemp}
          key={eventTemp.id}
          selectEvent={selectEvent}
          deleteEvent={deleteEvent}
        />
      ))}
    </>
  );
}

export default EventList;
