import React from "react";
import { List, Image } from "semantic-ui-react";

function EventListAttendee({ attendee }) {
  return (
    <List.Item>
      {/* <p>Attendee</p> */}
      <Image size="mini" circular src={attendee.photoURL}></Image>
    </List.Item>
  );
}

export default EventListAttendee;
