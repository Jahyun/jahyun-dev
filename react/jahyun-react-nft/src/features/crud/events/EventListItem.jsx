import React from "react";
import { Segment, Item, Icon, List, Button } from "semantic-ui-react";
import EventListAttendee from "./EventListAttendee";

function EventListItem({ event, selectEvent, deleteEvent }) {
  return (
    <Segment.Group>
      <Segment>
        <Item.Group>
          <Item>
            <Item.Image size="tiny" circular src={event.hostPhotoURL} />
            <Item.Content>
              <Item.Header content={event.title} />
              <Item.Description> Hosted by {event.hostedBy} </Item.Description>
            </Item.Content>
          </Item>
        </Item.Group>
      </Segment>
      <Segment>
        <span>
          <Icon name="clock" /> {event.date}
          <Icon name="marker" /> {event.venue}
        </span>
      </Segment>
      <Segment>
        <List horizontal>
          {/* SampleData 내에 attendees 항목이 있는데 array이므로 map 으로 모두 가져온다 */}
          {event.attendees.map((attendeeTemp) => (
            <EventListAttendee key={attendeeTemp.id} attendee={attendeeTemp} />
          ))}
        </List>
      </Segment>
      <Segment clearing>
        <div style={{marginBottom:"1.5em"}}>{event.description}</div>
        <Button
          onClick={() => deleteEvent(event.id)}
          color="red"
          floated="right"
          marginRight="2em"
          content="Delete"
        />
        <Button
          onClick={() => selectEvent(event)}
          color="teal"
          floated="right"
          marginRight="10px">
          View
        </Button>
      </Segment>
    </Segment.Group>
  );
}

export default EventListItem;
