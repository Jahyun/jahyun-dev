import React, { useState } from "react";
import cuid from "cuid";
import { Form, Label, Segment, Header, Button } from "semantic-ui-react";

function EventForm({ setFormOpen, createEvent, selectedEvent, updateEvent }) {
  // selectedEvent가 null 값이 아니면 selectedEvent를 넣고, Null 이면 그 아래 데이터를 넣는다는 의미
  const initialValue = selectedEvent ?? {
    title: "",
    category: "",
    description: "",
    city: "",
    venue: "",
    date: "",
  };
  const [values, setValues] = useState(initialValue);

  function handleFormSubmit() {
    // selectEvent가 Null이 아니면 (true), updateEvent 함수 실행
    // updateEvent ; ...slectedEvent 모두 유지 + ...values 입력한 데이터 모두 overwrite
    // 만약 selectedEvent가 Null 이면 (false), createEvent 실행
    selectedEvent
      ? updateEvent({ ...selectedEvent, ...values })
      : createEvent({
          ...values,
          id: cuid(),
          hostedBy: "Jahyun",
          attendees: [],
          hostPhotoURL: "/assets/user.png",
        });
    // form에 없는 input 항목은 간단히 값을 넣어 주었다
    // id 생성을 위해서 "cuid" 라이브러리를 npm install해서 사용했다
    setFormOpen(false);
  }

  function handleInputChange(e) {
    const { name, value } = e.target;

    setValues({ ...values, [name]: value });
    // { ...values, [name]: value }  values 기존 데이터 모두 가져오고, name 필드만 추가한다는 의미
    //** setValue() 내에서 ... 사용할 때,  {   } 로 감싼 것에 주의. EventDashboard.jsx에서는 [    ]를 활용했다.  */
  }

  return (
    <Segment clearing>
      {/* selectedEvent에 내용이 있으면 (true) "Edit the Event" 보여준다. */}
      <Header>{selectedEvent ? "Edit the Event" : "Create New Event"}</Header>

      <Form>
        <Form.Field>
          <Label basic ribbon style={{ width: "120px" }}>
            Title
          </Label>
          <input
            type="text"
            placeholder="Title"
            name="title"
            value={values.title}
            onChange={(e) => handleInputChange(e)}
          />
          {/* value, onChange 이벤트 함수 등을 추가한 후에, name 항목을 추가하지 않으면 입력칸에 타이핑이 안된다 */}
        </Form.Field>
        <Form.Field>
          <Label basic ribbon style={{ width: "120px" }}>
            Category
          </Label>
          <input
            type="text"
            placeholder="Category"
            name="category"
            value={values.category}
            onChange={(e) => handleInputChange(e)}
          />
        </Form.Field>
        <Form.Field>
          <Label basic ribbon style={{ width: "120px" }}>
            Description
          </Label>
          <input
            type="text"
            placeholder="Description"
            name="description"
            value={values.description}
            onChange={(e) => handleInputChange(e)}
          />
        </Form.Field>
        <Form.Field>
          <Label basic ribbon style={{ width: "120px" }}>
            City
          </Label>
          <input
            type="text"
            placeholder="City"
            name="city"
            value={values.city}
            onChange={(e) => handleInputChange(e)}
          />
        </Form.Field>
        <Form.Field>
          <Label basic ribbon style={{ width: "120px" }}>
            Venue
          </Label>
          <input
            type="text"
            placeholder="Venue"
            name="venue"
            value={values.venue}
            onChange={(e) => handleInputChange(e)}
          />
        </Form.Field>
        <Form.Field>
          <Label basic ribbon style={{ width: "120px" }}>
            Date
          </Label>
          <input
            type="date"
            placeholder="Date"
            name="date"
            value={values.date}
            onChange={(e) => handleInputChange(e)}
          />
        </Form.Field>
        <Button
          type="submit"
          floated="right"
          positive
          onClick={() => {
            handleFormSubmit();
          }}>
          Submit
        </Button>
        <Button
          type="submit"
          floated="right"
          onClick={() => {
            setFormOpen(false);
          }}>
          Cancel
        </Button>
      </Form>
    </Segment>
  );
}

export default EventForm;
