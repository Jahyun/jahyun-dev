import React from "react";
import { Form, Button } from "semantic-ui-react";

const CreateForm = () => (
  <Form>
    <Form.Group widths="equal">
      <Form.Field label="An HTML <input>" control="input" />
      <Form.Field label="An HTML <select>" control="select">
        <option value="male">Male</option>
        <option value="female">Female</option>
      </Form.Field>
    </Form.Group>
    <Form.Group grouped>
      <label>HTML radios</label>
      <Form.Field
        label="This one"
        control="input"
        type="radio"
        name="htmlRadios"
      />
      <Form.Field
        label="That one"
        control="input"
        type="radio"
        name="htmlRadios"
      />
    </Form.Group>
    <Form.Group grouped>
      <label>HTML checkboxes</label>
      <Form.Field label="This one" control="input" type="checkbox" />
      <Form.Field label="That one" control="input" type="checkbox" />
    </Form.Group>
    <Form.Field label="An HTML <textarea>" control="textarea" rows="3" />
    <Form.Checkbox label="I agree to the Terms and Conditions" />
    <Button type="submit">Submit</Button>
  </Form>
);

export default CreateForm;

// import React, { useState } from "react";

// const EventPractice = () => {
//   const [form, setForm] = useState({
//     username: "",
//     message: "",
//   });
//   const { username, message } = form;

//   const onChange = (e) => {
//     setTimeout(() => console.log(e), 500);
//     const nextForm = {
//       ...form, // 기존의 form 내용을 이 자리에 복사 한 뒤
//       [e.target.name]: e.target.value, // 원하는 값을 덮어씌우기
//     };
//     setForm(nextForm);
//   };
//   const onClick = () => {
//     alert(username + ": " + message);
//     setForm({
//       username: "",
//       message: "",
//     });
//   };
//   const onKeyPress = (e) => {
//     if (e.key === "Enter") {
//       onClick();
//     }
//   };
//   return (
//     <div>
//       <h1>이벤트 연습</h1>
//       <input
//         type="text"
//         name="username"
//         placeholder="유저명"
//         value={username}
//         onChange={onChange}
//       />
//       <input
//         type="text"
//         name="message"
//         placeholder="아무거나 입력해보세요"
//         value={message}
//         onChange={onChange}
//         onKeyPress={onKeyPress}
//       />
//       <button onClick={onClick}>확인</button>
//     </div>
//   );
// };
// export default EventPractice;
