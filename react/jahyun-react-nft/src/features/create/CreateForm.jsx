import React, { Component } from "react";
import "../../app/layout/styles.css";
import {
  Button,
  Header,
  Container,
  Form,
  Input,
  Radio,
  Select,
  TextArea,
} from "semantic-ui-react";

// 콤보박스 옵션 정의
const options = [
  { key: "m", text: "Male", value: "male" },
  { key: "f", text: "Female", value: "female" },
  { key: "o", text: "Other", value: "other" },
];

class CreateForm extends Component {
  state = {};

  handleChange = (e, { value }) => this.setState({ value });

  render() {
    const { value } = this.state;
    return (
      <Container
        secondary
        style={{
          backgroundColor: "#eaeaea",
          paddingTop: 20,
          paddingBottom: 50,
          paddingLeft: 20,
          paddingRight: 20,
        }}>
        <Container
          textAlign="left"
          style={{
            marginTop: 20,
            marginBottom: 50,
            marginLeft: 100,
            marginRight: 50,
          }}>
          <Header as="h1">Create New NFT</Header>
          <p>
            {" "}
            Fill out the following information to create the NFT for your item.
          </p>
        </Container>
        <Container textAlign="left">
          <Form>
            <Form.Field
              control={Input}
              label="Name"
              placeholder="Item Name"
              //   width="10px"
            />
            <Form.Field
              control={Select}
              label="Category"
              placeholder="Select Category"
            />
            <Form.Field
              control={Input}
              label="External Link"
              placeholder="External Link"
            />
            <Form.Field
              control={TextArea}
              rows="5"
              label="Description"
              placeholder="Provide detailed descriptions of your item."
            />
            <Form.Field
              control={Select}
              label="Properties"
              options={options}
              placeholder="Properties"
            />
            <Form.Field
              control={Input}
              label="Supply"
              placeholder="Number of Items"
            />

            {/* <Form.Group>
              <Form.Field
                control={Input}
                label="Supply"
                placeholder="Number of Items"
              />
              <Form.Field
                control={Select}
                label="Category"
                placeholder="Select Category"
              />
            </Form.Group> */}
            <Form.Group inline>
              <label>Blockchain</label>
              <Form.Field
                control={Radio}
                label="Ethereum"
                value="1"
                checked={value === "1"}
                onChange={this.handleChange}
              />
              <Form.Field
                control={Radio}
                label="Klaytn"
                value="2"
                checked={value === "2"}
                onChange={this.handleChange}
              />
              {/* <Form.Field
                control={Radio}
                label="Three"
                value="3"
                checked={value === "3"}
                onChange={this.handleChange}
              /> */}
            </Form.Group>

            <Form.Checkbox label="I agree to the Terms and Conditions" />
            <Button primary type="submit">
              Submit
            </Button>
          </Form>
        </Container>
      </Container>
    );
  }
}

export default CreateForm;
