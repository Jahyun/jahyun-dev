import React from "react";
import { Grid, Header } from "semantic-ui-react";
import CardItem from "./CardItem";
import styles from "./GalleryList.module.css";

function GalleryList({ dummyData }) {
  return (
    <>
      <div div className={styles.title}>
        <Header as="h1">Gallery</Header>
      </div>
      <container>
        <Grid centered>
          <Grid.Row padded>
            {dummyData.map((card) => (
              <CardItem card={card} key={card.id} />
            ))}
          </Grid.Row>
        </Grid>
      </container>
    </>
  );
}

export default GalleryList;
