import React from "react";
import { Card, Icon, Image } from "semantic-ui-react";
import styles from "./CardItem.module.css";

// props 대신 {items, i} 을 사용한 후, 이 변수를 그대로 사용함
function CardItem({ card }) {
  return (
    <div className={styles.card}>
      <Card>
        <Image
          className={styles.image}
          src={card.imageLocation}
          wrapped
          ui={false}
        />

        <div className={styles.content}>
          <Card.Content>
            <Card.Header>{card.title}</Card.Header>
            <Card.Meta>
              <span className="date">{card.name}</span>
            </Card.Meta>
            <Card.Description>{card.description}</Card.Description>
          </Card.Content>
          <Card.Content extra>
            <a href="foo">
              <Icon name="like" />
              {card.likes}
            </a>
          </Card.Content>
        </div>
      </Card>
    </div>
  );
}

export default CardItem;
