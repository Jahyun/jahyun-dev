import React, { useEffect, useState } from "react";
import { Button } from "semantic-ui-react";
import { ethers } from "ethers";
import styles from "./ConnectWallet.module.css";

const ConnectWallet = () => {
  const [errorMessage, setErrorMessage] = useState(null);
  const [account, setAccount] = useState(null);
  const [balance, setBalance] = useState(null);
  const [isConnect, setIsConnect] = useState(false);

  useEffect(() => {
    if (window.ethereum) {
      window.ethereum.on("accountsChanged", accountsChanged);
      window.ethereum.on("chainChanged", chainChanged);
    }
  }, []);

  const connectHandler = async () => {
    if (window.ethereum) {
      try {
        const res = await window.ethereum.request({
          method: "eth_requestAccounts",
        });
        await accountsChanged(res[0]);
        // {account === null ? setIsConnect(false) : setIsConnect(true)};
        setIsConnect(true);
      } catch (err) {
        console.error(err);
        setErrorMessage(
          "Metamask 지갑 연결 문제 발생! Metamask가 잠겨있는지 확인하고 지갑이 사용가능하도록 로그인해 주세요."
        );
      }
    } else {
      setErrorMessage("Install MetaMask");
    }
  };

  const accountsChanged = async (newAccount) => {
    setAccount(newAccount);
    try {
      const balance = await window.ethereum.request({
        method: "eth_getBalance",
        params: [newAccount.toString(), "latest"],
      });
      setBalance(ethers.utils.formatEther(balance));
    } catch (err) {
      console.error(err);
      setErrorMessage(
        "Metamask 지갑을 연결하는데 문제가 발생했습니다. Metamask가 잠겨있는지 확인하고 지갑이 사용가능하도록 로그인해 주세요."
      );
    }
  };

  const chainChanged = () => {
    setErrorMessage(null);
    setAccount(null);
    setBalance(null);
  };

  return (
    <div>
      <h1 style={{ marginTop: "50px" }}> Connect to Metamask wallet</h1>
      <h4> {"Connection to MetaMask using window.ethereum methods"}</h4>
      {/* <Button style={{marginTop: '20px'}} primary onClick={connectHandler}>Connect Account</Button> */}
      {!isConnect && (
        <Button style={{ marginTop: "20px" }} primary onClick={connectHandler}>
          {" "}
          Connect Account{" "}
        </Button>
      )}

      {isConnect && (
        <>
          <h4 className={styles.status}>Wallet is Connected</h4>{" "}
          <Button color="orange"> Disconnect </Button>
        </>
      )}

      {errorMessage ? <p>Error: {errorMessage}</p> : null}
      <div style={{ margin: "20px" }}>
        <div className={styles.box}>
          <h3> Account address: {account}</h3>
        </div>
        <div className={styles.box}>
          <h3>
            {" "}
            Balance: {balance} {balance ? "ETH" : null}
          </h3>
        </div>
      </div>
    </div>
  );
};

export default ConnectWallet;
