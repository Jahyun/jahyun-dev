import React, { useEffect, useState } from "react";
import { Button } from "semantic-ui-react";
import { ethers } from "ethers";
import styles from './MetaConnectPage.module.css';

const MetaConnect = () => {
  const [errorMessage, setErrorMessage] = useState(null);
  const [account, setAccount] = useState(null);
  const [balance, setBalance] = useState(null);

  useEffect(() => {
    if (window.ethereum) {
      window.ethereum.on("accountsChanged", accountsChanged);
      window.ethereum.on("chainChanged", chainChanged);
    }
  }, []);

  const connectHandler = async () => {
    if (window.ethereum) {
      try {
        const res = await window.ethereum.request({
          method: "eth_requestAccounts",
        });
        await accountsChanged(res[0]);
      } catch (err) {
        console.error(err);
        setErrorMessage("There was a problem connecting to MetaMask");
      }
    } else {
      setErrorMessage("Install MetaMask");
    }
  };

  const accountsChanged = async (newAccount) => {
    setAccount(newAccount);
    try {
      const balance = await window.ethereum.request({
        method: "eth_getBalance",
        params: [newAccount.toString(), "latest"],
      });
      setBalance(ethers.utils.formatEther(balance));
    } catch (err) {
      console.error(err);
      setErrorMessage("There was a problem connecting to MetaMask");
    }
  };

  const chainChanged = () => {
    setErrorMessage(null);
    setAccount(null);
    setBalance(null);
  };

  return (
    <div  >
      <h1 style={{marginTop: '50px'}}> Connect to Metamask wallet</h1>
      <h4> {"Connection to MetaMask using window.ethereum methods"}</h4>
      <Button style={{marginTop: '20px'}} primary onClick={connectHandler}>Connect Account</Button>
        {errorMessage ? (
          <p >
            Error: {errorMessage}
          </p> ) : null}  
          <div style={{margin:"20px"}}> 
      <div className={styles.box}>
        <h3> Account address: {account}</h3>
      </div>
      <div className={styles.box}>
        <h3> Balance: {balance} {balance ? "ETH" : null}</h3>
      </div>
      </div>
    </div>

    // <Paper elevation={3} sx={{ p: 3 }}>
    //   <Stack spacing={2}>
    //     <Typography variant="h6"> Account: {account} </Typography>
    //     <Typography variant="h6">
    //       Balance: {balance} {balance ? "ETH" : null}
    //     </Typography>
    //     <Button onClick={connectHandler}>Connect Account</Button>
    //     {errorMessage ? (
    //       <Typography variant="body1" color="red">
    //         Error: {errorMessage}
    //       </Typography>
    //     ) : null}
    //   </Stack>
    // </Paper>
  );
};

export default MetaConnect;