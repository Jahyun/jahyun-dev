// import React from "react";

const DummyData = [
  {
    id: "e1",
    imageLocation: "/assets/images/art1.jpg",
    title: "Cloudy Flows",
    name: "Danish Pan",
    description: "Imagine the clouds flowing in the sky",
    date: new Date(2020, 7, 14),
    likes: 22,
  },
  {
    id: "e2",
    imageLocation: "/assets/images/art2.jpg",
    title: "Woods",
    name: "Smith Paw",
    description: "Tress in my garden",
    date: new Date(2020, 10, 10),
    likes: 5,
  },
  {
    id: "e3",
    imageLocation: "/assets/images/art3.jpg",
    title: "Rocks at gate",
    name: "Timothy Gaia",
    description: "rocks block the gate to the new world",
    date: new Date(2021, 3, 4),
    likes: 8,
  },
  {
    id: "e4",
    imageLocation: "/assets/images/art4.jpg",
    title: "Hills",
    name: "Tomas Sting",
    description: "Enjoy the beautiful scenary",
    date: new Date(2021, 9, 14),
    likes: 10,
  },
];

export default DummyData;
