import React, {useState} from 'react';
import './App.css';
import './app/layout/styles.css';
import NavBar from './features/nav/NavBar';
import GalleryList from './features/gallery/GalleryList';
import DummyData from './app/api/Data';
import CreateForm from './features/create/CreateForm';
import Trade from './features/Trade';
import Home from './features/Home';
import TestPage1 from './features/TestPage1';
import CrudModule from './features/crud/CrudModule';
import ConnectWallet from './features/wallet/ConnectWallet';
import MetaConnectPage from './features/wallet/MetaConnectPage';


function App() {

  const [selectedMenu, setSelectedMenu] = useState("Home");

  return (
    <div className="App">
      
      {selectedMenu === "Home" && 
        <Home setSelectedMenu={setSelectedMenu} />
        }

      
        
        <NavBar setSelectedMenu={setSelectedMenu} />

        

        {selectedMenu === "Gallery" && 
        <GalleryList dummyData={DummyData}/>
        }
        {selectedMenu === "Create" && 
        <CreateForm />
        }
        {selectedMenu === "Trade" && 
        <Trade />
        }
        {selectedMenu === "TestPage1" && 
        <TestPage1 />
        }
        {selectedMenu === "CRUD Module" && 
        <CrudModule />
        }
        {selectedMenu === "MetaConnectPage" && 
        <MetaConnectPage />
        }
        {selectedMenu === "ConnectWallet" && 
        <ConnectWallet />
        }
     
     
   
    
    </div>
  );
}

export default App;
