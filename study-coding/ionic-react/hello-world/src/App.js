import {IonButton} from '@ionic/react';
import React from 'react';

function App() {
  return (
    <div>
      <header>
        <h1>My App</h1>
      </header>
      <main>
        {/* <p>Add some content here…</p> */}
        <IonButton>Click Me</IonButton>
      </main>
    </div>
  );
}

export default App;
