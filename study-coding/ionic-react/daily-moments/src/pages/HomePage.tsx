import {
  IonCard,
  IonContent,
  IonHeader,
  IonItem,
  IonList,
  IonPage,
  IonRouterLink,
  IonTitle,
  IonToolbar,
} from '@ionic/react';
import React from 'react';
import { Link } from 'react-router-dom';
import {entries} from '../data'

const HomePage: React.FC = () => {
  return (
    <IonPage>
      <IonHeader> 
        <IonToolbar>
          <IonTitle>Home</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent className="ion-padding">
        <IonList >
          {entries.map((entry) => 
            <IonItem  button key={entry.id} routerLink={`/entries/${entry.id}`}> 
              {entry.title} 
            </IonItem>
          )}
        </IonList>
        {/* This is HomePage... <br/><br/> */}
        {/* Go to <IonRouterLink routerLink="/home"> Home </IonRouterLink> */}
      </IonContent>
    </IonPage>
  );
};

export default HomePage;
