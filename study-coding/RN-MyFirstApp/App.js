import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View,TouchableOpacity, TextInput, Alert } from 'react-native';

export default class App extends React.Component {
  render() {
    let content =' '
    return(
      <View style={styles.container}>
        <TextInput 
          placeholder="여기에 텍스트를 입력하세요"
          onChangeText={(text)=>{
            content = text
          }}
          style={styles.input}
        />
        <TouchableOpacity
          activeOpacity={0.8}>
            onPress={()=> { Alert.alert (content)}}  
            {/*  버튼을 눌렀을때만 경고창이 뜨도록 한다 */}
        <View style={styles.box}>
          <Text style={styles.first}>Open up App.js to start working on your app!</Text>
          <StatusBar style="auto" />
        </View>
        </TouchableOpacity>
      </View>
     ) ;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  box: {
    flexDirection: 'row',
    backgroundColor: '#FFFFF',
    padding: 16,
  },
  first: {
    fontSize: 20,
    color: '#6830CF',
    fontWeight: '600',  // fontsize는 작은 따옴표 사용해야 함
    padding: 16, // 안쪽 여백
  },
  input: {
    width: 100%,

  }
});
