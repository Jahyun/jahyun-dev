// const router = require('./routes/courses');
const Joi = require('joi');
const _ = require('lodash');
const bcrypt = require('bcrypt');
const express = require('express');
const router = express.Router();

router.post ('/', async(req, res) => {
    const {error} = validate(req, res);
    if (errors) return res.status(400).send(error.detail[0].message);

    let user = await User.findOne({email: req.body.email});
    if (!user) return res.status(400).send('Invalid email or password.');


    bcrypt.compare(req.body.password, user.password)
    // user = new User(_.pick(req.body, ['name', 'email', 'password]));
    // const salt = await bcrypt.genSalt(10);
    // const hashed = await bcrypt.hash(user.password, salt);
    // await user.save();

    res.send(_.pick(user, ['_id', 'name', 'email']));
})


function validate(req) {
    const schema = {
        email: Joi.string().min(5).max(255).required().email(),
        password: Joi.string().min(5).max(255).required()
    };
    return Joi.validate(req, schema);
}

module.exports = router;