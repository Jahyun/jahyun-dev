const express = require('express');
const router = express.Router();

// api endpoint URL에 따라서 어떠한 response를 보여줄 것인지 정의함

// app.get('/', (req, res) => {
//     res.send('Hello World');
// });

router.get('/', (req, res) => {
    res.render('index', {title: 'My Express App', message: 'Hello'});
});

module.exports = router;