const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Joi = require('joi');


const courseSchema = new mongoose.Schema({
    name: {
        type: String, 
        required: true,  
        minlength: 5,
        maxlength: 255,
    },  
});
const Course = mongoose.model('Course', courseSchema); 

//POGETST ******************* GET ***************************/
//** Handling HTTP GET Request = Reading Data */
// router.get('/api/courses', (req, res) => {
router.get('/', async (req, res) => {
    const courses = await Course.find().sort('name');
    res.send(courses);
});

//POST ******************* POST ***************************/
//** Handling HTTP POST Request = Creating Data */
router.post('/', async (req, res) => { 
    const { error } = validateCourse(req.body); // error = result.error
    if (!course) return res.status(404).send('The course with the given ID was not found.');  // 404 = object not found

    let course = new Course (req.body);    // course object 생성 
    course = await course.save();    // course object 내용을 저장
    res.send(course);
});

//PUT ****************** PUT ***************************/
//** Handling HTTP PUT Request = Updating Data */
router.put('/:id', async (req,res) => {
    const { error } = validateCourse(req.body); // error = result.error
    if (error) return res.status(400).send(error.details[0].message);
    
    const course = await Course.findByIdAndUpdate(req.params.id, {name: req.body.name}, {
        new: true
    })
    if (!course) return res.status(404).send('The course with the given ID was not found.');  
    
    res.send(course);
})


//DELETE ****************** DELETE ***************************/
//** Handling HTTP DELETE Request = Deleting Data */
router.delete ('/:id', async (req, res) => {
    const course = await Course.findByIdAndDelete(req.params.id);
 
    if (!course) return res.status(404).send('The course with the given ID was not found.');  

    res.send(course);
})

function validateCourse(course){
    // define schema - 최소한의 input이 가져야 할 요건 정의
    const schema = {
        name: Joi.string().min(3).required()
    }
    // const result = Joi.validate(req.body, schema); // old logic
    return Joi.validate(course, schema);  // validate 결과를 내보낸다
}

//*****************************************************/
//* id에 해당되는 것을 보여주고, 없다면 404 에러 메시지 보여준다
router.get('/:id', async (req, res) => {
    const course = await Course.findById(req.params.id)

    if (!course) return res.status(404).send('The course with the given ID was not found.');  // 404 = object not found
    res.send(course);
})

// // http://localhost:3000/api/courses/2018/3  -> { "year": "2018",  "month": "3" }
// // http://localhost:3000/api/courses/2018/3?sortBy=name ; 이름으로 sort하도록 함
// router.get('/api/courses/:year/:month', (req, res) => {
//     res.send(req.params);
//     // res.send(req.query);
// })

module.exports = router;