const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const courseSchema = new mongoose.Schema({
    name: {
        type: String, 
        required: true,  
        minlength: 5,
        maxlength: 255,
    },  
});
const Course = mongoose.model('Course', courseSchema); 

//POGETST ******************* GET ***************************/
//** Handling HTTP GET Request = Reading Data */
// router.get('/api/courses', (req, res) => {
router.get('/', async (req, res) => {
    const courses = await course.find().sort('name');
    res.send(courses);
});

//POST ******************* POST ***************************/
//** Handling HTTP POST Request = Creating Data */
// client로부터의 input을 믿을 수 없으므로 input validation 필요
// http://localhost:3000/api/courses   + { "name": "course4" } >>> {"id": 4, "name": "course4"}
// 결과확인: http://postman.com 

router.post('/', (req, res) => {
    //* validate input    
    //* if invalid, return 400 - bad request    
    const { error } = validateCourse(req.body); // error = result.error
    if (!course) return res.status(404).send('The course with the given ID was not found.');  // 404 = object not found

    // if (!course) {   // 위 코드와 같이 refactoring 할 수 있다
    //     res.status(404).send('The course with the given ID was not found.');  // 404 = object not found
    //     return;
    // } 

    //* input 문제 없으면, 다음과 같이 data 생성한다.
    const course = {
        id: courses.length + 1, // id 추가
        name: req.body.name  // 
    };
    courses.push(course);  // array 맨 뒤에 추가
    res.send(course);
});

//PUT ****************** PUT ***************************/
//** Handling HTTP PUT Request = Updating Data */
//* 기존의 data를 input한 data로 replace 해 버린다 = 기존 data가 그냥 바뀐다
//* http://localhost:3000/api/courses/2 + {"name": "new course88"}  >>> { "id": 2, "name": "new course88"}

router.put('/:id', (req,res) => {
    //look up the course 
    // if not existing, return 404  (다른 곳에서 코드 복사해 온다)
    let course = courses.find(c => c.id === parseInt(req.params.id));
    if (!course) return res.status(404).send('The course with the given ID was not found.');  // 404 = object not found 

    //* validate,   
    //* if invalid, return 400 - bad request  (다른 곳에서 코드 복사해 온다)
    const { error } = validateCourse(req.body); // error = result.error
    if (error) return res.status(400).send(error.details[0].message);

    //* update course -> return the updated course
    course.name = req.body.name;
    res.send(course);
})


//DELETE ****************** DELETE ***************************/
//** Handling HTTP DELETE Request = Deleting Data */
//** id에 해당하는 data를 삭제한다 */
router.delete ('/:id', (req, res) => {
    // Look up the course
    // Not existing, return 404
    let course = courses.find(c => c.id === parseInt(req.params.id));
    if (!course) return res.status(404).send('The course with the given ID was not found.');  // 404 = object not found

    //* Delete
    const index = courses.indexOf(course); 
    courses.splice(index, 1);

    //* Return the same course
    res.send(course);
})

function validateCourse(course){
    // define schema - 최소한의 input이 가져야 할 요건 정의
    const schema = {
        name: Joi.string().min(3).required()
    }
    // const result = Joi.validate(req.body, schema); // old logic
    return Joi.validate(course, schema);  // validate 결과를 내보낸다
}

//*****************************************************/
//* id에 해당되는 것을 보여주고, 없다면 404 에러 메시지 보여준다
router.get('/:id', (req, res) => {
    let course = courses.find(c => c.id === parseInt(req.params.id));
    if (!course) return res.status(404).send('The course with the given ID was not found.');  // 404 = object not found
    res.send(course);
})

// http://localhost:3000/api/courses/2018/3  -> { "year": "2018",  "month": "3" }
// http://localhost:3000/api/courses/2018/3?sortBy=name ; 이름으로 sort하도록 함
router.get('/api/courses/:year/:month', (req, res) => {
    res.send(req.params);
    // res.send(req.query);
})

module.exports = router;