//**  Asynchronous Code 사용하는 법 배우기 */

console.log('Before');

//**  callback function 사용 사례 */

//** callback ; 여러개의 callback 함수가 nested된 복잡한 구조  */
// getUser(1, (user) => {   
//     //  console.log('User', user); // getUser의 callback 함수 (user) 실행
//     getRepositories(user.gitHubUsername, (repos) => {
//         console.log('Repos', repos); // getRepositories의 콜백함수 (repos) 실행
//         getCommits(repo, (commits) => {
//              console.log(commits);
//         })
//     })
// });

//START********* callback Hell 해결방향 */
//** 더 이상 nested callback 구조가 아니다. 별도의 함수의 list로 보인다 */

getUser(1, getRepositories);
// 아래에 같은 이름의 getRepositories가 있지만 서로 다르다
function getRepositories(user){   // 이 함수는 array: user를 arg로 가짐
    getRepositories(user.gitHubUsername, getCommits);  // 이 함수는 string, callback을 arg로 가짐
}
function getCommits(repos){
    getCommits(repo, displayCommits)   // displayCommits는 함수를 부르지 않는다
}
function displayCommits (commits){
    console.log(commits);
}
//END ********* callback Hell 해결방향 */

console.log('After');

function getUser(id, callback){
    setTimeout(() => { 
        console.log('Reading a user form a database...');
        callback({id: id, gitHubUsername: 'mosh'}) ;
    }, 2000);  // 2초 후에 함수를 실행
}

function getRepositories(username, callback) {
    setTimeout(() => {
        console.log('Calling GitHGub API...');
        callback( ['repo1', 'repo2', 'repo3']);
    },2000)
}