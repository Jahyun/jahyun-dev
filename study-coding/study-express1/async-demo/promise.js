//******  Promise 관련 코드 */

//** */ Promise는 변수 2개 (resolve, reject)를 가지며, 이것들은 모두 함수
//** */ Promise 속성 = object 

const p = new Promise((resolve, reject) => {  
    // Kick-off some async work -> 결과로 value or error 얻게 될 것임
    setTimeout(() => {
        resolve(1); // promise: pending => resolved or fulfilled
        reject(new Error('message')); // pending => rejected
    },2000);
});    

p
    .then(result => console.log('Result', result))   // 끝에 , ; 등 없다
    .catch(err => console.log('Error', err.message));
