//**  Asynchronous Code  */

console.log('Before');

//******** 1번째 방법 : Callback functions ***********/
//***** 아래 callback hell 구조를 promise를 사용하면 위와 같이 simple해 진다 */
// getUser(1, (user) => {   
//     getRepositories(user.gitHubUsername, (repos) => {
//         getCommits(repo, (commits) => {
//             console.log(commits);
//         })
//     })
// });

//******** 2번째 방법 : Replacing callbacks with Promises ***********/
// Promise 사용하였기 때문에 하나씩 fulfilled 결과를 확인하면서 다음 .then 으로 넘어간다
// getUser(1)
//     .then(user => getRepositories(user.gitHubUsername))
//     .then(repos => getCommits(repos[0]))
//     .then(commits => console.log('Commits', commits))
//     .catch(err => console.log('Error', err.message));

//******** 3번째 방법 : Async and Await ***********/
// await를 사용하면 위 Promise사용한 경우와 같이 하나씩 결과를 확인하면서 다음으로 넘어간다 
async function displayCommits() {
    try {
    const user = await getUser(1);
    const repos = await getRepositories(user.gitHubUsername);
    const commits = await getCommits(repos[0]);
    console.log(commits);
    }
    catch (err) {  
        console.log('Error', err.message);
    }
}
displayCommits();

console.log('After');

//** callback 함수를 사용한 사례 */
// function getUser(id, callback){
//     setTimeout(() => { 
//         console.log('Reading a user form a database...');
//         callback({id: id, gitHubUsername: 'mosh'}) ;
//     }, 2000);  // 2초 후에 함수를 실행
// }

//***** Replacing Callbacks with Promises */
// getUser 함수의 callback을 없애고 promise 사용한다
function getUser(id) {
    return new Promise((resolve, reject) => {
        //Kick off some async work
        setTimeout(() => { 
            console.log('Reading a user form a database...');
            resolve({id: id, gitHubUsername: 'mosh'}) ;
        }, 2000);  // 2초 후에 함수를 실행
    });
}

//***** Replacing Callbacks with Promises */
function getRepositories(username) {
    return new Promise((resolve, reject) => {  // Promise의 전형적인 표현법
        setTimeout(() => {
            console.log('Calling GitHGub API...');
            resolve ( ['repo1', 'repo2', 'repo3']);
            // reject(new Error(('Could not get the repos...'))) // error 발생의 경우
        },2000);
    });
}

function getCommits(repo) {
    return new Promise((resolve, reject) => {  // Promise의 전형적인 표현법
        setTimeout(() => {
            console.log('Calling GitHGub API...');
            resolve ( ['Commit']);
        },2000);
    });
}