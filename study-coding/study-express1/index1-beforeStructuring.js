const express = require('express');
const debug = require('debug')('app:startup');
// const config = require('config');
const logger = require('./logger');
const morgan = require('morgan');
const helmet = require('helmet');
const Joi = require('joi');
const app = express();

// Templating Engine 사용 (PUG)
// EJS, Mustache 등 다른 engine도 있음
app.set('view engine', 'pug');   // require 불필요
app.set('views', './views');   // views 폴더를 default folder로 활용

// Configuration :  아래 코드는 에러 발생했음 - 해결 못했음
// console.log('Application Name: ', config.get('name'));
// console.log('Mail Server: ', config.get('mail.host'));
// console.log('Mail Server: ', config.get('mail.password'));

// Environments 관련 middleware 활용
// console.log(`NODE_ENV: ${process.env.NODE_ENV}`);
// console.log(`app: ${app.get('env')}`);  // development
if(app.get('env')==='development'){  // 개발모드에서만 morgan 함수를 실행하자
    app.use(morgan('tiny'));  
    // console.log('Morgan enabled...');
    debug('Morgan enabled...');  // debug용으로 console.log() 대신 사용 
}

// json 관련한 기능 사용하도록 middleware 설정
app.use(express.json());  // req.body 

// key=value&key=value   url encoded input을 처리할 수 있게 함
// {extended:true}는 array 등도 처리할 수 있게 함
app.use(express.urlencoded({extended:true})); 

// Express 내의 built-in middleware 활용하기
// public 폴더 내의 static file 전달
// http://host:3000/readme.txt >>> public folder 내의 readme.txt 내용이 나타난다
app.use(express.static('public'));

// 3rd party middleware 활용하기
app.use(helmet());
// app.use(morgan('tiny'));  // middleware 실행하는데 소요되는 response time 체크한다

// custom middleware function 생성 방법
// logger.js에서 function을 가져와서 middleware 생성
app.use(logger);


// configuration 세팅하는데 활용하는 middleware : npm config
// https://www.npmjs.com/package/config




// middleware function for authentication
app.use(function(req, res, next) {
    console.log('Authenticating...');
    next();
})

const courses = [
    { id: 1, name: 'course1'},
    { id: 2, name: 'course2'},
    { id: 3, name: 'course3'},
]

// api endpoint URL에 따라서 어떠한 response를 보여줄 것인지 정의함
// app.get('/', (req, res) => {
//     res.send('Hello World');
// });
app.get('/', (req, res) => {
    res.render('index', {title: 'My Express App', message: 'Hello'});
});

//POGETST ******************* GET ***************************/
//** Handling HTTP GET Request = Reading Data */
app.get('/api/courses', (req, res) => {
    res.send(courses);
});

//POST ******************* POST ***************************/
//** Handling HTTP POST Request = Creating Data */
// client로부터의 input을 믿을 수 없으므로 input validation 필요
// http://localhost:3000/api/courses   + { "name": "course4" } >>> {"id": 4, "name": "course4"}
// 결과확인: http://postman.com 

app.post('/api/courses', (req, res) => {
    //* validate input    
    //* if invalid, return 400 - bad request    
    const { error } = validateCourse(req.body); // error = result.error
    if (!course) return res.status(404).send('The course with the given ID was not found.');  // 404 = object not found

    // if (!course) {   // 위 코드와 같이 refactoring 할 수 있다
    //     res.status(404).send('The course with the given ID was not found.');  // 404 = object not found
    //     return;
    // } 

    //* input 문제 없으면, 다음과 같이 data 생성한다.
    const course = {
        id: courses.length + 1, // id 추가
        name: req.body.name  // 
    };
    courses.push(course);  // array 맨 뒤에 추가
    res.send(course);
});

//PUT ****************** PUT ***************************/
//** Handling HTTP PUT Request = Updating Data */
//* 기존의 data를 input한 data로 replace 해 버린다 = 기존 data가 그냥 바뀐다
//* http://localhost:3000/api/courses/2 + {"name": "new course88"}  >>> { "id": 2, "name": "new course88"}

app.put('/api/courses/:id', (req,res) => {
    //look up the course 
    // if not existing, return 404  (다른 곳에서 코드 복사해 온다)
    let course = courses.find(c => c.id === parseInt(req.params.id));
    if (!course) return res.status(404).send('The course with the given ID was not found.');  // 404 = object not found 

    //* validate,   
    //* if invalid, return 400 - bad request  (다른 곳에서 코드 복사해 온다)
    const { error } = validateCourse(req.body); // error = result.error
    if (error) return res.status(400).send(error.details[0].message);

    //* update course -> return the updated course
    course.name = req.body.name;
    res.send(course);
})


//DELETE ****************** DELETE ***************************/
//** Handling HTTP DELETE Request = Deleting Data */
//** id에 해당하는 data를 삭제한다 */
app.delete ('/api/courses/:id', (req, res) => {
    // Look up the course
    // Not existing, return 404
    let course = courses.find(c => c.id === parseInt(req.params.id));
    if (!course) return res.status(404).send('The course with the given ID was not found.');  // 404 = object not found

    //* Delete
    const index = courses.indexOf(course); 
    courses.splice(index, 1);

    //* Return the same course
    res.send(course);
})

function validateCourse(course){
    // define schema - 최소한의 input이 가져야 할 요건 정의
    const schema = {
        name: Joi.string().min(3).required()
    }
    // const result = Joi.validate(req.body, schema); // old logic
    return Joi.validate(course, schema);  // validate 결과를 내보낸다
}

//*****************************************************/
//* id에 해당되는 것을 보여주고, 없다면 404 에러 메시지 보여준다
app.get('/api/courses/:id', (req, res) => {
    let course = courses.find(c => c.id === parseInt(req.params.id));
    if (!course) return res.status(404).send('The course with the given ID was not found.');  // 404 = object not found
    res.send(course);
})

// http://localhost:3000/api/courses/2018/3  -> { "year": "2018",  "month": "3" }
// http://localhost:3000/api/courses/2018/3?sortBy=name ; 이름으로 sort하도록 함
app.get('/api/courses/:year/:month', (req, res) => {
    res.send(req.params);
    // res.send(req.query);
})

//* 환경변수 설정 = PORT
const port = process.env.PORT || 3000 ;
app.listen(port, () => console.log(`Listening on port ${port}....`))