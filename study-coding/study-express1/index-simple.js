const express = require('express');
const debug = require('debug')('app:startup');
// const config = require('config');
const logger = require('./middleware/logger');
const courses = require('./routes/courses')
const home = require('./routes/home')
const auth = require('./routes/auth');
const morgan = require('morgan');
const helmet = require('helmet');
const Joi = require('joi');
const app = express();

app.set('view engine', 'pug');   // require 불필요
app.set('views', './views');   // views 폴더를 default folder로 활용


app.use(express.json());  // req.body 
app.use(express.urlencoded({extended:true})); 

// Express 내의 built-in middleware 활용하기
// public 폴더 내의 static file 전달
// http://host:3000/readme.txt >>> public folder 내의 readme.txt 내용이 나타난다
app.use(express.static('public'));
app.use(helmet());      // 3rd party middleware 활용하기
app.use(logger);    // custom middleware function 생성 방법  (middleware 폴더 내 파일)

// 모든 router에 대해서 api url 설정
// courses.js 내에 있는 '/api/courses'을 '/' 로 변경하면 더 심플하다
app.use('/api/courses', courses);   
app.use('/', home);
app.use('/api/auth', auth);

// middleware function for authentication
app.use(function(req, res, next) {
    console.log('Authenticating...');
    next();
})

//* 환경변수 설정 = PORT
const port = process.env.PORT || 3000 ;
app.listen(port, () => console.log(`Listening on port ${port}....`))