const express = require('express');
const debug = require('debug')('app:startup');
// const config = require('config');
const logger = require('./middleware/logger');
const courses = require('./routes/courses')
const home = require('./routes/home')
const auth = require('./routes/auth');
const morgan = require('morgan');
const helmet = require('helmet');
const app = express();
const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/playground') 
    .then(() => console.log('Connected to MongoDB...'))
    .catch(err => console.error('Could not connect to MongoDB...'));

// Templating Engine 사용 (PUG) ;  EJS, Mustache 등 다른 engine도 있음
app.set('view engine', 'pug');   // require 불필요
app.set('views', './views');   // views 폴더를 default folder로 활용

// Environments 관련 middleware 활용
// console.log(`NODE_ENV: ${process.env.NODE_ENV}`);
// console.log(`app: ${app.get('env')}`);  // development
if(app.get('env')==='development'){  // 개발모드에서만 morgan 함수를 실행하자
    app.use(morgan('tiny'));  
    // console.log('Morgan enabled...');
    debug('Morgan enabled...');  // debug용으로 console.log() 대신 사용 
}

// json 관련한 기능 사용하도록 middleware 설정
app.use(express.json());  // req.body 

// key=value&key=value   url encoded input을 처리할 수 있게 함
// {extended:true}는 array 등도 처리할 수 있게 함
app.use(express.urlencoded({extended:true})); 


// public 폴더 내의 static file 전달
// http://host:3000/readme.txt >>> public folder 내의 readme.txt 내용이 나타난다
app.use(express.static('public'));// Express 내의 built-in middleware 활용하기
app.use(helmet());// 3rd party middleware 활용하기
app.use(logger);

// 모든 router에 대해서 api url 설정
// courses.js 내에 있는 '/api/courses'을 '/' 로 변경하면 더 심플하다
app.use('/api/courses', courses);   
app.use('/', home);
app.use('/api/auth', auth);

// middleware function for authentication
app.use(function(req, res, next) {
    console.log('Authenticating...');
    next();
})

//* 환경변수 설정 = PORT
const port = process.env.PORT || 3000 ;
app.listen(port, () => console.log(`Listening on port ${port}....`))