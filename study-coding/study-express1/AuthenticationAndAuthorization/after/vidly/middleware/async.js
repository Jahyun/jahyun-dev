
module.exports = function asyncMiddleware(handler) {   // handler는 3개 변수 가짐
    // next는 exeption 처리하는 error.js middleware 불러서 사용 
    // index.js에서 app.use(error)로 middleware 설정했음
    return async (req, res, next) => {   
      try {
        await handler(req, res);  
      }
      catch(ex) {   // ex = exception
        next(ex);
      }
    };
  }
  