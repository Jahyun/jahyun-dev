const winston = require('winston');

// index.js에서 middleware 리스트 마지막에 이 function을 부른다면...
// 이 내용을 다른 곳에서 next()로 불러서 사용할 수 있다.

module.exports = function(err, req, res, next){
    // Log the exeption
    winston.error(err.message, err); // err는 metadata를 의미함

  // error
  // warn
  // info
  // verbose
  // debug 
  // silly
  
    res.status(500).send('Something failed.');
  }