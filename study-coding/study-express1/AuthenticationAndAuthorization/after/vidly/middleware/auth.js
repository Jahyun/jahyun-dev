const jwt = require('jsonwebtoken');
const config = require('config');

//**  JASON  Web Token 사용하여 인증(authentication)  */
//**  JWT = digital signature 를 검증한 후, req.user에 넣는다 */
module.exports = function (req, res, next) {  // function 이름 없다 !
  const token = req.header('x-auth-token');  // header에 jwt가 있으니 가져와서 Token에 넣음
  if (!token) return res.status(401).send('Access denied. No token provided.');

  try {
    const decoded = jwt.verify(token, config.get('jwtPrivateKey'));  // jwt 검증
    req.user = decoded; // 검증되면 req.user에 jwt를 넣는다
    next(); // pass control to the next middleware function
  }
  catch (ex) {
    res.status(400).send('Invalid token.'); // 검증되지 않으면 에러메시지
  }
}