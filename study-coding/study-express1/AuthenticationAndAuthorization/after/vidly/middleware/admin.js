
  //** isAdmin이 true인지 확인하는 과정 */
  // authorization request function = req.user

module.exports = function (req, res, next) { 

  if (!req.user.isAdmin) return res.status(403).send('Access denied.');
  // 401 Unauthorized
  // 403 Forbidden 

  next();
}