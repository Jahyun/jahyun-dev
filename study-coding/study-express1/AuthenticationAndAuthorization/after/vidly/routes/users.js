const auth = require('../middleware/auth');
const jwt = require('jsonwebtoken');
const config = require('config');
const bcrypt = require('bcrypt');  // hasing passwords
const _ = require('lodash'); // 반복적인 표현 간략화
const {User, validate} = require('../models/user');
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();

//**  Getting the current User ; user 검색 */
router.get('/me', auth, async (req, res) => {
  const user = await User.findById(req.user._id).select('-password');
  res.send(user);
});

//*******  Register User   */
router.post('/', async (req, res) => {
  const { error } = validate(req.body);   // validate request   
  if (error) return res.status(400).send(error.details[0].message);
  // email가지고 User 기록 확인한 후, 기록이 있으면 Error message
  let user = await User.findOne({ email: req.body.email }); 
  if (user) return res.status(400).send('User already registered.'); 
  // user object에 정보를 담는다 - using Lodash (req.body.name 등으로 req.body.~ 반복을 없애는 기능)
  user = new User(_.pick(req.body, ['name', 'email', 'password'])); 
  // Hashing Passwords - using bcrypt
  const salt = await bcrypt.genSalt(10);
  user.password = await bcrypt.hash(user.password, salt);  
  await user.save();  
  
  const token = user.generateAuthToken();  // token 생성
  // res.send()에 header('x-auth-token', token)를 붙여서 보낸다
  res.header('x-auth-token', token).send(_.pick(user, ['_id', 'name', 'email']));
});

module.exports = router; 
