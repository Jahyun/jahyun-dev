const Joi = require('joi');
const bcrypt = require('bcrypt');
const _ = require('lodash');
const {User} = require('../models/user');
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();

//** Login을 위한 User Authentication 확인 */
router.post('/', async (req, res) => {
  const { error } = validate(req.body);  // 아래에서 정의한 validate 함수 사용
  if (error) return res.status(400).send(error.details[0].message);
  // email 등록되어 있는지 확인
  let user = await User.findOne({ email: req.body.email });
  if (!user) return res.status(400).send('Invalid email or password.');
  // password 맞는지 비교, 확인
  const validPassword = await bcrypt.compare(req.body.password, user.password);
  if (!validPassword) return res.status(400).send('Invalid email or password.');
  // JASON Web Token을 서명, 생성해서 저장함 - generateAuthToken 메소드 사용
  const token = user.generateAuthToken();
  res.send(token);
});

//** user.js에서 validate를 가지져오지 않고 별도로 함수를 정의한다 */
//** 여기서는 name 없이, email, password만 검증한다 */
function validate(req) {
  const schema = {
    email: Joi.string().min(5).max(255).required().email(),
    password: Joi.string().min(5).max(255).required()
  };

  return Joi.validate(req, schema);
}

module.exports = router; 
