require('winston-mongodb');
const winston = require('winston');
require('express-async-errors');

module.exports = function() {
    winston.handleExceptions (
        new winston.transports.Console({ colorize: true, prettyPrint: true}),
        new winston.transports.File({filename: 'uncaughtExceptions.log'})
      );
      
      process.on('unhandledRejection', (ex) => {
        throw ex;   // exception을 내 보내면, 위의 winston.handleExceptions가 자동으로 받아서 log 저장한다
      }); 
      
      winston.add(winston.transports.File, {filename: 'logfile.log'});
      winston.add(winston.transports.MongoDB, {
        db: 'mongodb://localhost/vidly',
        level: 'error'
      });
}