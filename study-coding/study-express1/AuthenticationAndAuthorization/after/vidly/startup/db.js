const winston = require('winston');
const mongoose = require('mongoose');

module.exports = function() {

    mongoose.connect('mongodb://localhost/vidly')
        // .then(() => console.log('Connected to MongoDB...'))
        .then(() => winston.info('Connected to MongoDB...'))

        // .catch(err => console.error('Could not connect to MongoDB...')); // 이것은 데모용으로 넣은 것이고 실제로는 필요없다

}