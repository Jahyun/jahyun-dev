const config = require('config');

module.exports = function() {
    //** config 사용해서 jwtPrivateKey 정의되어 있는지 validate */
    //** 터미널에서 $export vidly_jwtPrivateKey=mySecureKey 라고 명령어 실행한 후 */
    //** index.js를 실행하면 jwtPrivateKey 검증 에러 나지 않는다 */
    if (!config.get('jwtPrivateKey')) {
        // console.error('FATAL ERROR: jwtPrivateKey is not defined.');
        throw new Error('FATAL ERROR: jwtPrivateKey is not defined.');
        // process.exit(1);  // 0: failure  1: error
    }
}