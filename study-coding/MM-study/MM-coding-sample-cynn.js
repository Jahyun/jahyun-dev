import WSServer from '../../common/wsserver';
import ExchangeBase from './base';
import { Exchange, OrderSide, OrderType, Symbol } from '../../common/def';

import HTTPRequest from '../../common/http';
import ExecutionReport from "../trade/execution";
import Util from "../../common/util";
import Balance from "../trade/Balance";
import OrderBook from '../market/orderBook';
import * as crypto from 'crypto';
import * as moment from 'moment';
import { dir } from 'console';

const clients:any = {};

let lastTime:{[symbol:string]:number;} = {};

let pendingList:{[symbol:string]:number[];} = {};
let filledList:{[symbol:string]:number[];} = {};

export default class Foblgate extends ExchangeBase {

    static http = new HTTPRequest("https://api2.foblgate.com");

    //MMAcount
    accesskey = "236a4f434a9130ec";
    secret = "f07cc1025a1bdbf8";
    mbId = "allmedicoin1@gmail.com";


    startWS(ws:WSServer){
        this._ws = ws;
        
        this.recieveStream("AMDCKRW");
        //this.recieveStream("BTCUSDT");
    }

    private recieveStream(symbol: string) {
        setInterval(() => {
            this.getDepth(symbol);
        }   
            //obj.setIncrementData(symbol);
        , 1000);

    }

    public async getBalance() {

        const obj = this;
        var mbId = this.mbId;
        var payload = {apiKey:this.accesskey, mbId : mbId}

        var secretHash = crypto.createHash('sha256').update(this.accesskey + mbId + this.secret).digest('hex');
        return new Promise<void>(async function (resolved:any, rejected:any) {
            try{            
                
                var response : any = await Foblgate.http.postFoble("/api/account/balance", secretHash, payload );            
                var res = JSON.parse(response);

                let balance = new Balance();

            
                if(res.data != undefined){
                    balance.KRW = res.data.total.KRW;
                    balance.availKRW = res.data.avail.KRW;
                    balance.AMDC = res.data.total.AMDC;
                    balance.availAMDC = res.data.avail.AMDC;
            
                }

                resolved(balance);        


            }catch(e){
                console.log(e);
            }
        });
    }

    private convertSYMBOL(symbol:string){
        if(symbol == "AMDCKRW"){
            return "AMDC/KRW";
        }
        

        return symbol;
    }


    public async order(clOrderID: string, symbol: Symbol, type: OrderType, side: OrderSide, price: number, qty: number, tif: string) {
        const obj = this;

        let direction = "";
        if(side == OrderSide.buy){
            direction = "bid";
        }else if(side == OrderSide.sell){
            direction = "ask";
        }

        var mbId = this.mbId;
        var pairName = this.convertSYMBOL(symbol);
        var action = direction;
        var price = price;
        var amount = qty;
      
        var formData = {
         apiKey : this.accesskey,
         mbId : mbId,
         pairName : pairName,
         action : action,
         price : price,
         amount : amount,
        };

        obj.recoder.logSendOrder(JSON.stringify(formData));
        return new Promise<ExecutionReport>(async function (resolved:any, rejected:any) {

            const startTime = new Date().getTime();

            var secretHash = crypto.createHash('sha256').update(obj.accesskey + mbId + pairName + action + price + amount + obj.secret).digest('hex');
            var response : any = await Foblgate.http.postFoble("/api/trade/orderPlace", secretHash, formData );            
            var res = JSON.parse(response);

            var executionReport = new ExecutionReport();
            executionReport.clOrderID = clOrderID;

            if (res.data != undefined && res.status == "0") {                        
                executionReport.orderID = res.data;
                executionReport.status = 0;
                obj.updateOrderList(symbol);
            } else {                        
                executionReport.orderID = "failed";        
                executionReport.filledQty = 0;
                executionReport.avgPrice = 0;
                executionReport.status = -1;

                obj.recoder.logError(JSON.stringify(formData) + " / " + response);                
            }

            resolved(executionReport);

            
            obj.recoder.logReceiveOrder(JSON.stringify(response), startTime);
            
        });
    };


    public async updateOrderList(symbol: string) {

        var mbId = this.mbId;
        var pairName = this.convertSYMBOL(symbol);
        var action = "all";
        var cnt = 100;
        var skipIdx = 0;
      
        var formData = {
         apiKey : this.accesskey,
         mbId : mbId,
         pairName : pairName,
         action : action,
         cnt : cnt,
         skipIdx : skipIdx
        };

        this.recoder.logSendOrder(JSON.stringify(formData));
        
        var secretHash = crypto.createHash('sha256').update(this.accesskey + mbId + pairName + action + cnt + skipIdx + this.secret).digest('hex');
        var response : any = await Foblgate.http.postFoble("/api/account/signHistory", secretHash, formData );
        var resComplete = JSON.parse(response);

 
        var response : any = await Foblgate.http.postFoble("/api/account/notSignBalance", secretHash, formData );
        var resPending = JSON.parse(response);

        if(resComplete.status == "0"){
            filledList[symbol] = [];
            for(let i = 0; i < resComplete.data.list.length; ++i){
                filledList[symbol].push(resComplete.data.list[i].ordNo)
            }
        }


        if(resPending.status == "0"){
            pendingList[symbol] = [];
            for(let i = 0; i < resPending.data.list.length; ++i){
                pendingList[symbol].push(resPending.data.list[i].ordNo)
            }
        }


        lastTime[symbol] = new Date().getTime();
        return 0;

    }

    public async orderInfo(symbol: string, orderId: string, side: string) {

        if(lastTime[symbol] == undefined){
            lastTime[symbol] = 0;
        }
  
        if(new Date().getTime() - lastTime[symbol] > 4000){
            await this.updateOrderList(symbol);
        }

     
        const obj = this;
        
        return new Promise<ExecutionReport>(async function (resolved:any, rejected:any) {
            
            const startTime = new Date().getTime();

            var executionReport = new ExecutionReport();
            executionReport.orderID = orderId;
            executionReport.status = -1;

            let applyed = false;
            let iOrderId = parseInt(orderId);

            for(let i = 0; i < filledList[symbol].length; ++i){
                if(filledList[symbol][i] == iOrderId){
                    applyed = true;
                    executionReport.status = 2;
                }
            }

            if(applyed == false){
                for(let i = 0; i < pendingList[symbol].length; ++i){
                    if(pendingList[symbol][i] == iOrderId){                        
                        executionReport.status = 1;
                    }
                }   
            }

            resolved(executionReport);
        });
    }

    public async getDepth(symbol: string) {
        const obj = this;
        var payload = {apiKey:this.accesskey, pairName: this.convertSYMBOL(symbol)}
        try{            
            var secretHash = crypto.createHash('sha256').update(this.accesskey + this.convertSYMBOL(symbol) + this.secret).digest('hex');
            var response : any = await Foblgate.http.postFoble("/api/ticker/orderBook", secretHash, payload );   
	    console.log("a:" + response);
	    var res = JSON.parse(response);

            if (res.data != undefined) {   
                var orderBook = new OrderBook(symbol);

                const buy = res.data.buyList;
                const sell = res.data.sellList;

                for (var i = 0; i < buy.length && i < 20; i++) {

                    var price = parseFloat(buy[i].price);
                    var quantity = parseFloat(buy[i].amount);

                    //var price = parseFloat(bookBuy.keys()[i].price);
                    //var quantity = parseFloat(data.data.bids[i].quantity);
                    orderBook.addBid(price, quantity);
                }

                for (var i = 0; i < sell.length && i < 20; i++) {

                    var price = parseFloat(sell[i].price);
                    var quantity = parseFloat(sell[i].amount);

                    //var price = parseFloat(bookBuy.keys()[i].price);
                    //var quantity = parseFloat(data.data.bids[i].quantity);
                    orderBook.addAsk(price, quantity);
                }

                this.broadcastOderBook(orderBook);
            }
        }catch(e){
            console.log(e);
        }
    }

    public async getVolume(symbol: string) {
        const obj = this;

        var pairName = this.convertSYMBOL(symbol);
        var type = 1;
        var min = 0;
        var startDateTime = moment().format('YYYYMMDDHHmmss');
        var cnt = 2;

        var payload = {apiKey:this.accesskey, pairName: pairName, type :type, min:min, startDateTime:startDateTime, cnt:cnt }
        try{  
            
            var secretHash = crypto.createHash('sha256').update(this.accesskey + pairName + type + min + startDateTime + cnt + this.secret).digest('hex');            
            var response : any = await Foblgate.http.postFoble("/api/chart/selectChart", secretHash, payload );            
            var res = JSON.parse(response);

            let data = res.data.series[0].split('|');

            return {result : data[5]};
            

        }catch(e){
            console.log(e);
        }

        return {result : "0"};
    }


    public async orderCancel(symbol: string, orderId: string, side: string, price: number, qty: number) {
        const obj = this;

        
        let direction = "";
        if(side.toLocaleLowerCase() == OrderSide.buy.toLocaleLowerCase()){
            direction = "bid";
        }else if(side.toLocaleLowerCase() == OrderSide.sell.toLocaleLowerCase()){
            direction = "ask";
        }
                
        var mbId = this.mbId;
        var pairName = this.convertSYMBOL(symbol);
        var ordNo = orderId;
        var action = direction;
        var ordPrice = price;

        var formData = {
        apiKey : this.accesskey,
        mbId : mbId,
        pairName : pairName,
        ordNo : ordNo,
        action : action,
        ordPrice : ordPrice
        };


        obj.recoder.logSendOrder(JSON.stringify(formData));
        return new Promise<ExecutionReport>(async function (resolved:any, rejected:any) {
            
            const startTime = new Date().getTime();

            var secretHash = crypto.createHash('sha256').update(obj.accesskey + mbId + pairName + ordNo + action + ordPrice + obj.secret).digest('hex');
            var response : any = await Foblgate.http.postFoble("/api/trade/orderCancel", secretHash, formData );            
            var res = JSON.parse(response);

            var executionReport = new ExecutionReport();
            

            if (res.status == "0") {                                        
                executionReport.status = -1;
            } else {                        		        
                executionReport.status = 1;
                obj.recoder.logError("cancel error:" + JSON.stringify(formData) + " / " + response);                
            }

            resolved(executionReport);

            obj.recoder.logReceiveOrder(JSON.stringify(response), startTime);

            
        });
    }




    constructor() {
        super(Exchange.foblgate);
      
    }



}