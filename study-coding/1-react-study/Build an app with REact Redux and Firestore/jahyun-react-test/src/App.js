import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        
        <p>
          여기에 나름대로 NFT marketplace와 유사한 Website 구축을 해 보자
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React Test...
        </a>
      </header>
    </div>
  );
}

export default App;
