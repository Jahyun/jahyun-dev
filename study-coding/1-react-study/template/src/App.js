/*eslint-disable*/       
// 이것 써 넣으면 terminal에서 문법적 warning 메시지들이 더 이상 나오지 않아서 유용하다

import React, {useState} from 'react';   // useState라는 내장함수를 사용한다는 의미
import logo from './logo.svg';  
// 아래애서 이 그림파일을 불러오려면 <img src={logo} /> 식으로 {}를 사용해서 불러오면 된다
import './App.css';

function App() {

  return (
    <p> Hello, this is a template !!! </p>
 )
}

export default App;
