import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';  // app.js에서 가져온다는 의미

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')  // index.html의 id=root 를 가져온다는 의미
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
