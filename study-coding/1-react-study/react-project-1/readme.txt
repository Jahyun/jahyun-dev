
React 강의 (코딩애플) 의 샘플 코딩 소스


React 기초 0강 : 리액트왜 쓰는지 알려줌 (+ 수강시 필요 사전지식)

[https://www.youtube.com/watch?v=LclObYwGj90&t=3s](https://www.youtube.com/watch?v=LclObYwGj90&t=3s)

React 기초 1강 : 리액트 설치와 셋팅법 (2021+ 스타일)

[https://www.youtube.com/watch?v=nahwuaXmgt8](https://www.youtube.com/watch?v=nahwuaXmgt8)

React 기초 2강 : 리액트에선 HTML대신 JSX 써야함 (JSX 사용법)

[https://www.youtube.com/watch?v=FqnAFX9lQPQ](https://www.youtube.com/watch?v=FqnAFX9lQPQ)

React 기초 3강 : 리액트에선 변수말고 state 만들어 쓰랬죠 (useState)

[https://www.youtube.com/watch?v=Qb8Oiy8i9IY&t=55s](https://www.youtube.com/watch?v=Qb8Oiy8i9IY&t=55s)

React 기초 4강 : 리액트에서 버튼에 이벤트 리스너 (핸들러) 장착하는 법

[https://www.youtube.com/watch?v=Br9fKSIeAok](https://www.youtube.com/watch?v=Br9fKSIeAok)

React 기초 5강 : state 맘대로 변경하는 법 (setState는 넘 옛날이고염)

[https://www.youtube.com/watch?v=CowLAnmhxMY](https://www.youtube.com/watch?v=CowLAnmhxMY)

React 기초 6강 : Component로 HTML 깔끔하게 줄이는 법

[https://www.youtube.com/watch?v=bq2SjODrhJQ](https://www.youtube.com/watch?v=bq2SjODrhJQ)