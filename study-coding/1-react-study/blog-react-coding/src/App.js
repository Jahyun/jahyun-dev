
/*eslint-disable*/       
// 이것 써 넣으면 terminal에서 문법적 warning 메시지들이 더 이상 나오지 않아서 유용하다

import React, {useState} from 'react';   // useState라는 내장함수를 사용한다는 의미
import logo from './logo.svg';  
// 아래애서 이 그림파일을 불러오려면 <img src={logo} /> 식으로 {}를 사용해서 불러오면 된다
import './App.css';

function App() {
  
  // state 정의
  let [글제목, aChange] = useState(['남자코트','우동맛집']);  
  // ES6 destructuring 문법. '글제목'이라는 변수에 값(array)을 넣는다
  // 2번째 있는 변수는 값을 변경하기 위한 함수 ; 글제목을 바로 바꾸지 않고 다른 array로 값을 공유한 후에 이 array 값을 바꾸고 나서 
  // useState 사용하는 이유: State의 내용이 바뀌면 새로고침 없이도 html이 재렌더링이 되기 때문임
  // 그래서 언제나 변경이 될 수 있는 데이터는 무조건 useState를 사용해서 적용하는 것이 좋다
  let [좋아요, bChange] = useState(0);  //2번째 변수인 "좋아요변경"은 변경을 위한 변수 역할을 한다
  
  function changeTitle (){
    // var newArray = 글제목;  이것은 동일한 값을 공유하는 것이고 복사하는 방식이 아니다
    //******** state를 바로 변경하면, 재렌더링이 안될 수 있기 때문에 별도의 독립적인 복사본을 만든 후에 변경해야 한다 (값 공유인 경우에는 변경 NO) *** 중요한 포인트임
    // 값 공유가 아니라 "독립적인 값을 가지는 복사"를 하기 위해서는 deep copy라는 방식을 사용한다
    // 아래의 ... 는 ES6 신문법으로서 spread operator인데 서로 독립적인 값을 가진 별개의 array로 deep copy하는 방식이 된다
    // ...이라는 spread operator는 (), {} 등을 모두 없애달라는 의미. 이런 기호를 없앤 후 []에 담으면 별개의 array가 된다
    var newArray = [...글제목];
    // 복사를 한 array를 활용하면.... 이제는 변경할 수 있다.
    newArray[0] =  '여자코트';
    // 이제 'bChange' 함수를 사용해서 변경한 별도의 array 값을 가지고 글제목 array를 replace해 주면 된다
    //******** 즉, bChange는 state 변경을 위한 함수의 역할을 한다 
    aChange(newArray);
    // state 변경함수를 사용할 때 넣는 변수는 state의 변수와 동일한 포맷으로 넣는다 (값, array, object 등)
  }
  
  //***** 리액트의 대원칙 = immutable data = 데이터는 직접 수정이 되면 안된다.
  
  let posts = '강남 고기 맛집';
  let abc = {color:'yellow', fontSize:'30px'};
  
  return ( 
    < div className="App" >  
    {/* JSX에서는 class 대신 className을 사용 */}

      <div className = "black-nav">
        {/* syle은 무조건 {}를 사용해야 함.  위에 있는 abc라는 object 내용을 불러오고 있음 */}
        <div style={abc}>개발 Blog</div>   
      </div>   

      <div className="list">
        {/* span에다 함수를 정의해서 넣는다. eventListener, eventHandler 함수는 이렇게 사용한다 */}
        <h3> {글제목[0]} <span onClick = { ()=>{ bChange( 좋아요 +1) }}> 👌 </span> {좋아요}  </h3>
        {/* 손가락 아이콘을 클릭하면 좋아요=0 에 1을 더한 후에 {좋아요} 위치에 변경된 숫자를 보여준다 */}
        {/* {좋아요}를 보여주고 있는데, bChange( 좋아요 +1)로 좋아요 값을 1 증가시키면 자동으로 재랜더링되어 {좋아요} 값이 1 추가되어 보여진다. */}
        {/* 예를 들면,  bChange (100) 을 실행하면 {좋아요} 값이 100으로 변경되어 보여진다. */}

        <p> 2월17일 발행</p> 

        {/* 줄 긋기 */}
        <hr/> 

        {/* click했을 때 {} 안에는 함수를 넣어야 한다. 
        ()=>{ 수식 등 } 를 사용해서 함수로 만들어서 넣을 수도 있다  */}
        <button onClick ={ changeTitle }> 제목변경 </button>


      {/* div가 많은 것이 싫으면 component를 사용한다 */}
      {/* <div className="modal">
        <h2> 제목 </h2>
        <p> 날짜 </p>
        <p> 상세내용 </p>
      </div> */}
     
     
      <Modal></Modal>
     
      {/* 아래에 function Moal()을 만든 후에 이것을 Component로서 사용한다. 
      이 경우 component 이름은 대문자로 시작하는 것이 관례다 */}

    
      </div>   
  );
}

{/* // function App()과 동일한 레벨에서 Component를 만들어서 사용한다.
// Component에 html 덩어리를 넣어서 사용한다
// 반복적으로 표현되는 내용은 component로 치환하면 도움이 된다.
// 자주 바뀌는 UI도 componet로 만드는 것이 좋다
// component 안에서 state를 사용하는 경우에는 복잡해짐 - 변수의 범위 때문에 props 문법을 사용해서 가져와서 사용해야 함 */}

function Modal(){
  return(
    <div className="modal">
        <h2> 제목 </h2>
        <p> 날짜 </p>
        <p> 상세내용 </p>
    </div>
  )
}

export default App;

