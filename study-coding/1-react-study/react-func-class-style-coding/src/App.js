import React from 'react';
import './App.css';

function App() {
    return(
        <div className="container">
            <h1>Hello! there...!!! </h1>
            <FuncComp initNumber={2}></FuncComp>
            <ClassComp initNumber={2}></ClassComp>
            
        </div>

    );
}

// props 대신 다른 변수명을 사용해도 되는데 props라고 사용한 것임

function FuncComp (props){
    return(
        <div >
            <h2>function style component</h2>
            <p>Number: {props.initNumber} </p>   
        </div>
    );
}

class ClassComp extends React.Component{
    state={
        number:this.props.initNumber
    }
    render(){
        return (
            <div className="container">
            <h2>class style component</h2>
            <p>Number: {this.state.number} </p>   
            <input type="button" value="random" onclick = {
                function(){
                    this.setState({number:Math.random()})
                }.bind(this)
            }> </input>
            </div>
        )
    }
}

export default App;

