import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';  // app.js에서 가져온다는 의미

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')  // index.html의 id=root 를 가져온다는 의미
);