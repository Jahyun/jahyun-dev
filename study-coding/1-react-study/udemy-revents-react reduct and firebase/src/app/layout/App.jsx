/*eslint-disable*/       
// 이것 써 넣으면 terminal에서 문법적 warning 메시지들이 더 이상 나오지 않아서 유용하다

import React from 'react';   // useState라는 내장함수를 사용한다는 의미
import EventDashboard from '../../features/events/eventDashboard/EventDashboard';


function App() {
  return (
    <div >
      <h1>Re-vents</h1>
      <EventDashboard />
    </div>
 )
}

export default App;
