const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/playground')  // promise를 돌려준다.
    .then(() => console.log('Connected to MongoDB...'))
    .catch(err => console.error('Could not connect to MongoDB...'));

const courseSchema = new mongoose.Schema({
    name: String,
    author: String,
    tags: [String],
    data: { type:Date, default: Date.now},
    isPublished: Boolean,
});

// class = human 이라면  object는  John = new Human 식으로 생성한다
const Course = mongoose.model('Course', courseSchema); // Course는 class라서 대문자로 시작


async function getCourses(){

    const courses1 = await Course.find();

    console.log('course1:', courses1);

}

getCourses();

