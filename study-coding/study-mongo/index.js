const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/playground') 
    .then(() => console.log('Connected to MongoDB...'))
    .catch(err => console.error('Could not connect to MongoDB...'));

const courseSchema = new mongoose.Schema({
    name: {
        type: String, 
        required: true,  // name is required (이런 방식은 mongoDB에만 적용)
        minlength: 5,
        maxlength: 255,
    },  
    category: {
        type: String,
        required: true,
        enum: ['web', 'mobile', 'network'],
        lowercase: true,   // 소문자로만 입력
        trim: true
    },
    author: String,
    tags: {
        type: Array,
        validate: {
            validator: function(v) {
                return v && v.length > 0;   
                // tags 값이 null이 아니면서 길이가 0보다 클 때 빠져나오고, 그렇지 않다면 message 전달
            },
            message: 'A course should have at least one tag.'
        }
    },
    data: { type:Date, default: Date.now},
    isPublished: Boolean,
    price: {
        type: Number,
        required: function(){ return this.isPublished;},
        min: 10,
        max: 200,
        get: v => Math.round(v),  // data 가져올 때 적용
        set: v => Math.round(v),  // data 쓸 때 적용
    }
});
const Course = mongoose.model('Course', courseSchema); 

// 여기 까지는 공통 초기 세팅 ************************************

async function createCourse(){
    const course = new Course ({    // course는 Object라서 소문자로 시작
        name: 'Angular Course',
        category: 'Web',
        author: 'John',
        tags: ['front-end'],
        isPublished: true,
        price: 15.8
    })
    
    try {
        const result = await course.save();  // promise 값을 주기 때문에 await 적용
        console.log(result);
    }
    catch (ex) {
        for (field in ex.errors)
        console.log(ex.errors[field].message);  
        // console.log(ex.message);
    }
};

async function getCourses(){
    const pageNumber = 2;
    const pageSize = 10;

    const courses4 = await Course
        .find().or( [{author:'Mosh'}, {isPublished: true}])
        .skip((pageNumber-1)*pageSize)  // pagination
        .limit(pageSize)
        .sort({name: 1})
        .select({name: 1, tags: 1})

    console.log('courses4:', courses4);
}

//**  query first  ; fundById()
//**  Modify its properties
//**  save()
async function updateCourse1(id){
    const course = await Course.findById(id);
    if (!course) return;
    course.isPublished = true;
    course.author = 'Another Author';

    // course.set({
    //     isPublished = true,
    //     author = 'Another Author'
    // })

    const result = await course.save();
    console.log(result);
}

//**  Approach: Update first */
//**  Update directly
//**  mongoDB update operator : https://docs.mongodb.com/manual/reference/operator/update/
async function updateCourse2(id){
    const result = await Course.update({ _id: id}, {
        $set: {
            author: 'Mosh',
            isPublished: false
        }
    });
    console.log(result);
}

//**  Approach: Update first + Update directly
//**  AND get the updated document
async function updateCourse3(id){
    const result = await Course.findByIdAndUpdate(id, {
        $set: {
            author: 'Jack',
            isPublished: false
        }
    });
    console.log(result);
}

//** remove data */
async function removeCourse1(id){

    // Course.deleteOne({ isPublished: true })
    const result = await Course.deleteOne({ _id: id})
    console.log(result);
}

async function removeCourse2(id){
    const result = await Course.findByIdAndRemove(id)
    console.log(result);
}

createCourse();
// getCourses();
// updateCourse1('6135e14f59a988a772508cd1');
// updateCourse2('6135e14f59a988a772508cd1');
// updateCourse3('6135e14f59a988a772508cd1');
// removeCourse2('6135e14f59a988a772508cd1');

