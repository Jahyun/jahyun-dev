const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/playground') 
    .then(() => console.log('Connected to MongoDB...'))
    .catch(err => console.error('Could not connect to MongoDB...'));

const courseSchema = new mongoose.Schema({
    name: String,
    author: String,
    tags: [String],
    data: { type:Date, default: Date.now},
    isPublished: Boolean,
});
const Course = mongoose.model('Course', courseSchema); 

// 여기 까지는 공통 초기 세팅 ************************************

async function getCourses(){
    const pageNumber = 2;
    const pageSize = 10;

    const courses4 = await Course
        .find().or( [{author:'Mosh'}, {isPublished: true}])
        .skip((pageNumber-1)*pageSize)  // pagination
        .limit(pageSize)
        .sort({name: 1})
        .select({name: 1, tags: 1})

    console.log('courses4:', courses4);
}

//**  query first  ; fundById()
//**  Modify its properties
//**  save()
async function updateCourse1(id){
    const course = await Course.findById(id);
    if (!course) return;
    course.isPublished = true;
    course.author = 'Another Author';

    // course.set({
    //     isPublished = true,
    //     author = 'Another Author'
    // })

    const result = await course.save();
    console.log(result);
}

//**  Approach: Update first */
//**  Update directly
//**  mongoDB update operator : https://docs.mongodb.com/manual/reference/operator/update/
async function updateCourse2(id){
    const result = await Course.update({ _id: id}, {
        $set: {
            author: 'Mosh',
            isPublished: false
        }
    });
    console.log(result);
}

//**  Approach: Update first + Update directly
//**  AND get the updated document
async function updateCourse3(id){
    const result = await Course.findByIdAndUpdate(id, {
        $set: {
            author: 'Jack',
            isPublished: false
        }
    });
    console.log(result);
}

//** remove data */
async function removeCourse1(id){

    // Course.deleteOne({ isPublished: true })
    const result = await Course.deleteOne({ _id: id})
    console.log(result);
}

async function removeCourse2(id){
    const result = await Course.findByIdAndRemove(id)
    console.log(result);
}

// updateCourse1('6135e14f59a988a772508cd1');
// updateCourse2('6135e14f59a988a772508cd1');
// updateCourse3('6135e14f59a988a772508cd1');
removeCourse2('6135e14f59a988a772508cd1');

