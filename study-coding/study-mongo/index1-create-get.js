const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/playground')  // promise를 돌려준다.
    .then(() => console.log('Connected to MongoDB...'))
    .catch(err => console.error('Could not connect to MongoDB...'));


const courseSchema = new mongoose.Schema({
    name: String,
    author: String,
    tags: [String],
    data: { type:Date, default: Date.now},
    isPublished: Boolean,
});

// class = human 이라면  object는  John = new Human 식으로 생성한다
const Course = mongoose.model('Course', courseSchema); // Course는 class라서 대문자로 시작

// 여기 까지는 공통 초기 세팅 ************************************

async function createCourse(){
    const course = new Course ({    // course는 Object라서 소문자로 시작
        name: 'Angular Course',
        author: 'John',
        tags: ['angular', 'frontend'],
        isPublished: true
    })
    
    const result = await course.save();  // promise 값을 주기 때문에 await 적용
    console.log(result);
};

async function getCourses(){
    const pageNumber = 2;
    const pageSize = 10;

    const courses4 = await Course
        .find().or( [{author:'Mosh'}, {isPublished: true}])
        .skip((pageNumber-1)*pageSize)  // pagination
        .limit(pageSize)
        .sort({name: 1})
        .select({name: 1, tags: 1})

    // comparison operator: eq(equal) ne(not equal) gt (greater than) gte lt lte in  nin(not in)
    // logical operator: or, and
    const courses3 = await Course
        // .find({author:'Mosh', isPublished: true})
        // .find({ author: /^Mosh/ }) // starts with Mosh
        // .find({ author: /Mosh$/i }) // ends with Mosh,  i = case insensitive
        // .find({ author: /.*Mosh.*/i }) // including Mosh
        // // .find({price: {$gt: 10, $lt: 20}});  10 < price < 20 인 data 선택
        // .find({price: { $in:[10, 15, 20] }});  price = 10, 15, 20 인 data 선택
        .find().or( [{author:'Mosh'}, {isPublished: true}])
        .limit(10)
        .sort({name: 1})
        .select({name: 1, tags: 1})

    const courses1 = await Course.find();
    const courses2 = await Course.find({author:'Mosh'});

    console.log('course1:', courses1);
    console.log('course2:', courses2);
    console.log('course3:', courses3);
    console.log('course4:', courses3);
}

// createCourse();
getCourses();

