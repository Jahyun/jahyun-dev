
console.log( "hello");


// challenge 1: Your Age in Days

function ageInDays() {
    let birthYear = prompt ('What year you were born?')
    let ageInDayss = (2021 - birthYear) * 365;
    let h1 = document.createElement('h1');
    let textAnswer = document.createTextNode('You are ' + ageInDayss + ' days old.');
    h1.setAttribute('id','ageInDays');
    h1.appendChild(textAnswer);
    document.getElementById('flex-box-result').appendChild(h1);
}

function reset(){
    document.getElementById('ageInDays').remove();
}

//Challenge 2 : generate Cat

function generateCat(){
    let image = document.createElement('img');
    let div = document.getElementById('flex-cat-gen');
    image.src = "https://images.apilist.fun/cats_api.png";
    div.appendChild(image);
}

///////////////////////////////////////////////////////////////
// Challenge 5 : Blackjack Game
///////////////////////////////////////////////////////////////

let blackjackGame = {
    // your-blackjack-result는 id라서 #을 붙였다
    'you': {'scoreSpan': '#your-blackjack-result', 'div': '#your-box', 'score': 0},
    'dealer': {'scoreSpan': '#dealer-blackjack-result', 'div': '#dealer-box', 'score': 0}, //object
    'cards': ['2','3','4','5','6','7','8','9','10','J','K','Q','A'], //array
    'cardsMap': {'2':2, '3':3,'4':4,'5':5,'6':6,'7':7,'8':8,'9':9,'10':10,'J':10,'K':10,'Q':10,'A':[1,11]}, // 각 카드를 숫자로 mapping함  A는 1 또는 11
    // 배점표 테이블 값 초기값 설정
    'wins': 0,
    'losses': 0,
    'draws': 0,
    // 버튼을 언제 어떻게 누를 수 잇고 못 누를지에 대해서 정하려면 state를 활용해야 한다
    'isStand': false, // Stand 버튼을 눌렀는가? 그렇다면 Hit 버튼 누를 수 없도록 해야 한다
    'turnsOver': false,  // 모든 카드를 다 뒤집었는가?  dealer 카드까지 다 뒤집어져서 이제 deal 버튼 누를 수 있는가?
}

// 변화하지 않을 변수라서 const 사용
const YOU = blackjackGame['you'];
const DEALER = blackjackGame ['dealer'];
const hitSound = new Audio('static/sounds/swish.m4a');
const winSound = new Audio('static/sounds/cash.mp3');
const lossSound = new Audio('static/sounds/aww.mp3');


// Hit, Deal 버튼을 클릭할 때 (ID 불러왔고) --> blackjactHit, blackjackDeal 등의 함수를 실행한다는 의미
document.querySelector("#blackjack-hit-button").addEventListener('click',blackjackHit);
document.querySelector("#blackjack-stand-button").addEventListener('click',dealerLogic);
document.querySelector("#blackjack-deal-button").addEventListener('click',blackjackDeal);

// You 영역에 카드를 보여준다
function blackjackHit(){
    if(blackjackGame['isStand']===false){
        let card = randomCard(); // card에 card 상의 숫자를 입력 (1,2,3, J, K, Q  등)
        console.log(card); // 무슨 카드를 선택되었는지 숫자를 컨솔에 보여준다
        showCard(card, YOU); // card를 보여준다
        updateScore(card, YOU); // score에 숫자를 더한다
        showScore(YOU);
    }

    // console.log(YOU['score']);

}

// cards 중에서 랜덤하게 임의로 하나를 선택
function randomCard(){
    let randomIndex =  Math.floor(Math.random()*13);
    return blackjackGame['cards'][randomIndex];
}

// 카드를 이미지 폴더에서 가져와서 보여주는 함수 
// card: 카드 수 (blackjackGame[cards][숫자]) 
// activePlayer : 어느 player의 영역에 카드를 보여줄 것인가?
function showCard(card, activePlayer){
    // 현재 score가 21보다 적을 경우에만 카드를 보여주고, 21이 넘으면 보여주지 않는다
    if (activePlayer['score'] <= 21) {
        // card 나타나는 동작.  cardImage에 원하는 카드 그림을 넣은 후 div 영역에 보여준다
        let cardImage = document.createElement('img');
        // ${}를 사용할 때에는 관련 전체 내용을 '' 로 하는 것이 아니라 `` (back-tag, Opt+)로 감싸야 한다
        cardImage.src = `static/images/${card}.png`;
        document.querySelector(activePlayer['div']).appendChild(cardImage);
        hitSound.play();
    }

}

// DEAL을 클릭했을 때...

function blackjackDeal(){
    if (blackjackGame['turnsOver']===true){
        // your-box 내의 모든 카드 아미지를 yourImages에 넣는다
        let yourImages = document.querySelector('#your-box').querySelectorAll('img');
        let dealerImages = document.querySelector('#dealer-box').querySelectorAll('img');
    
        // deal을 클릭하면 카드를 삭제한다
        for (i=0; i < yourImages.length; i++){
            yourImages[i].remove();
        }
        for (i=0; i < dealerImages.length; i++){
            dealerImages[i].remove();
        }
        // object 내에 있는 score 값을 다시 0으로 reset한다
        YOU['score'] = 0;
        DEALER['score'] = 0;
    
        // 그리고, scoreSpan에 보여지는 숫자들도 원래대로 돌려놓는다
        // Deal 버튼을 누르면, 위의 점수 판의 내용을 다시 원래 값으로 돌려놓는다.
    
        document.querySelector('#your-blackjack-result').textContent =0;
        document.querySelector('#dealer-blackjack-result').textContent =0;
        document.querySelector('#blackjack-result').textContent = "Let's Play";
    
        document.querySelector('#your-blackjack-result').style.color = 'white';
        document.querySelector('#dealer-blackjack-result').style.color = 'white';
        document.querySelector('#blackjack-result').style.color = 'black';

        blackjackGame['turnsOver']= false;
        blackjackGame['isStand']=false;
    }
    


}

// card: 1,2,J,Q 등의 카드 값 
function updateScore(card, activePlayer) { 
    // A인 경우, 11을 더할 때 21보다 적으면 더하고, 그렇지 않으면 1을 더한다
    if(card === 'A') {
        if ((activePlayer['score'] + blackjackGame['cardsMap'][card][1]) <= 21) {
            activePlayer['score'] += blackjackGame['cardsMap'][card][1];
        } else {
            activePlayer['score'] += blackjackGame['cardsMap'][card][0];
        }  

    } else {
        activePlayer['score'] += blackjackGame['cardsMap'][card];
    }
}


// activePlayer의 scoreSpan에 score에 합계가 나타나게 한다.
// activePlayer의 score
function showScore(activePlayer){
    if(activePlayer['score'] > 21) {
        document.querySelector(activePlayer['scoreSpan']).textContent = 'BUST!';
        document.querySelector(activePlayer['scoreSpan']).style.color = 'red';
    } else {
        document.querySelector(activePlayer['scoreSpan']).textContent = activePlayer['score'];
    }
}

// play 하는 과정에 약간의 시간 간격을 주는 함수
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

// Stand 버튼을 누르면 이 함수가 실행된다
async function dealerLogic(){
    blackjackGame['isStand']=true; // Stand 버튼을 눌렀다

    while (DEALER['score'] < 16 && blackjackGame['isStand']===true){
        let card = randomCard();
        showCard(card, DEALER); // card를 보여준다
        updateScore(card, DEALER); // score에 숫자를 더한다
        showScore(DEALER);  
        await sleep(3000);
    }
    // dealer 소계가 15 초과하면 자동으로 게임이 종료되도록 한다
        blackjackGame['turnsOver']=true; 
        // 누가 이겻는지를 판단한 computeWinner 결과를 가지고 위에서 showResuslt 메시지를 보여준다
        let winner = computeWinner();
        showResult(winner);
    
    
}

// winner를 정하는 판단 로직
// 배점판도 계산한다.
function computeWinner(){
    let winner;

    if(YOU['score'] <= 21) {
        if (YOU['score'] > DEALER['score'] || DEALER['score'] > 21) {
            blackjackGame['wins']++;
            console.log ('you won!');
            winner = YOU;
        } else if (YOU['score'] < DEALER['score']) {
            blackjackGame['losses']++;
            console.log ('you lost!');
            winner = DEALER ;
        } else if ( YOU['score'] === DEALER['score']) {
            blackjackGame['draws']++;
            console.log('you drew!')
        }
    } else if (YOU['score'] > 21 && DEALER['score'] <= 21) {
        blackjackGame['losses']++;
        console.log ('you lost!');
        winner = DEALER ;
    } else if (YOU['score'] > 21 && DEALER['score'] > 21) {
        blackjackGame['draws']++;
        console.log('you drew!')
    } 
    console.log(blackjackGame);
    return winner;
}

// winner가 누구인지에 따라서 
function showResult(winner) {
    let message, messageColor;

    if (blackjackGame['turnsOver']===true){
        if (winner === YOU) {
            document.querySelector('#wins').textContent = blackjackGame['wins'];
            message = 'You won!';
            messageColor = 'green';
            winSound.play();
        } else if (winner === DEALER) {
            document.querySelector('#losses').textContent = blackjackGame['losses'];
            message = 'You lost!';
            messageColor = 'red';
            lossSound.play();
        } else {
            document.querySelector('#draws').textContent = blackjackGame['draws'];
            message = 'You drew!';
            emssageColor = 'black';
        }
    
        document.querySelector('#blackjack-result').textContent = message;
        document.querySelector('#blackjack-result').style.color = messageColor;
    }

}
