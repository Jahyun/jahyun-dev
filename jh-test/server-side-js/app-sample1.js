/**  Main application file */

var express = require('express');
var path = require('path');
var app = express(); // 함수를 app이라는 변수로 사용 (이렇게 형식적으로 사용)


/**  Template Engine 사용 - start */
app.set('view engine', 'ejs'); // ejs라는 template engine 사용한다고 정의
app.set('views', path.resolve(__dirname, "views")); // views 폴더에 template file을 넣고 사용
// http://localhost:3000/test 으로 접근하면
// views 폴더 내에 index.ejs라는 파일을 실행
app.get('/test', function(req, res) {
    res.render("index", { apptitle: "EJS test file" }); // apptitle 변수에 문자열을 지정한다
});
/**  Template Engine 사용 - end */

// /** QueryString을 전달하는 것에 따라서 web app이 다르게 동작 */
// app.get('/topic', function(req, res) {
//     // res.send(req.query.id); // http://localhost:3000/topic?id=hello 를 접속하면 hello가 표시됨
//     res.send(req.query.id + ',' + req.query.name); //http://localhost:3000/topic?id=1&name=tom -> 1,tom 출력
// })

// /** semantic URL 방식 적용 */
// localhost:3000/topic/1/tom -> 1,tom 을 출력한다
app.get('/topic/:id/:name', function(req, res) {
    res.send(req.params.id + ',' + req.params.name);
})

/** QueryString 활용 사례 - Start */
app.get('/topic', function(req, res) {
        var topics = [
            'Javascript is...',
            'Nodejs is...',
            'Express is...'
        ];
        var output = `
    <a href="/topic?id=0"> JavaScript </a><br>
    <a href="/topic?id=1"> Nodejs </a><br>
    <a href="/topic?id=2"> Express </a><br><br>
    ${topics[req.query.id]}
    `
        res.send(output); // http://localhost:3000/topic?id=hello 를 접속하면 hello가 표시됨
    })
    /** QueryString 활용 사례 - END */


// 정적인 파일이 위치한 directory를 무엇으로 할지를 정의한다
// public 폴더에 정적 파일인 route.jpg 이미지 파일을 넣어 놓고 사용할 것이다
// http://localhost:3000/route 접속하면 public 폴더에 있는 이미지 파일을 보여준다
app.use(express.static('public'));
app.get('/route', function(req, res) {
    res.send('Heller Router, <img src="/route.jpg"></img>');
});




// homepage '/'로 localhost:3000 으로 접속하였을 때 어떠한 반응을 하게 한다
app.get('/', function(req, res) {
    res.send('<h1>Hello home page</h1>');
});

// http://localhost:3000/login 로 들어왔을 때 다음과 같은 반응을 하게 한다
app.get('/login', function(req, res) {
    res.send('Login please');
});

// 3000번 port에서 대기하도록 함
app.listen(3000, function() {
    console.log('connected with 3000 port!');
});