/**  Main application file */

var express = require('express');
var path = require('path');
var app = express(); // 함수를 app이라는 변수로 사용 (이렇게 형식적으로 사용)


/**  Template Engine 사용 - start */
app.set('view engine', 'jade'); // ejs라는 template engine 사용한다고 정의
app.set('views', path.resolve(__dirname, "views")); // views 폴더에 template file을 넣고 사용

/**  Template Engine 사용 - end */



/** QueryString 활용 사례 - Start */
app.get('/topic', function(req, res) {
        var topics = [
            'Javascript is...',
            'Nodejs is...',
            'Express is...'
        ];
        var output = `
    <a href="/topic?id=0"> JavaScript </a><br>
    <a href="/topic?id=1"> Nodejs </a><br>
    <a href="/topic?id=2"> Express </a><br><br>
    ${topics[req.query.id]}
    `
        res.send(output); // http://localhost:3000/topic?id=hello 를 접속하면 hello가 표시됨
    })
    /** QueryString 활용 사례 - END */



// /** semantic URL 방식 적용 */
// localhost:3000/topic/1/tom -> 1,tom 을 출력한다
app.get('/topic/:id/:name', function(req, res) {
    res.send(req.params.id + ',' + req.params.name);
})



// 정적인 파일이 위치한 directory를 무엇으로 할지를 정의한다
// public 폴더에 정적 파일인 route.jpg 이미지 파일을 넣어 놓고 사용할 것이다
// http://localhost:3000/route 접속하면 public 폴더에 있는 이미지 파일을 보여준다
app.use(express.static('public'));
app.get('/route', function(req, res) {
    res.send('Heller Router, <img src="/route.jpg"></img>');
});



// homepage '/'로 localhost:3000 으로 접속하였을 때 어떠한 반응을 하게 한다
app.get('/', function(req, res) {
    res.send('<h1>Hello home page</h1>');
});

// http://localhost:3000/login 로 들어왔을 때 다음과 같은 반응을 하게 한다
app.get('/login', function(req, res) {
    res.send('Login please');
});


app.get('/form', function(req, res) {
    res.render("form");
});

app.get('/form_receiver', function(req, res) {
    var title = req.query.title;
    var description = req.query.description;
    res.send(title + ',' + description);
})



// 3000번 port에서 대기하도록 함
app.listen(3000, function() {
    console.log('connected with 3000 port!');
});