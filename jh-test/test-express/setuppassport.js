var passport = require("passport");
var LocalStrategy = require("passport-local").Strategy;

var User = require("./models/user");

// 이 파일을 함수화했음  
// app.js에서 
// var setUpPassport = require("./setuppassport"); 
// setuppassport(); 실행할 수 있도록 함
module.exports = function() {

    // 사용자 아이디에서 사용자를 직렬화하고 역직렬화하는 방법을 지시한다
    // 사용자 개체를 아이디로 전환
    passport.serializeUser(function(user, done) {
        done(null, user._id);
    });
    // 아이디를 사용자 개체로 전환, 종료시 done 호출
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    // passport에 local strategy를 사용하도록 알려준다
    // 로그인 시도하는 아이디가 등록되어 있는지 확인하고
    passport.use("login", new LocalStrategy(function(username, password, done) {
        User.findOne({ username: username }, function(err, user) {
            if (err) { return done(err); }
            if (!user) {
                return done(null, false, { message: "등록된 아이디가 아닙니다!" });
            }
            // 비번이 맞는지 확인
            user.checkPassword(password, function(err, isMatch) {
                if (err) { return done(err); }
                if (isMatch) {
                    return done(null, user);
                } else {
                    return done(null, false, { message: "패스워드가 틀렸습니다!" });
                }
            });
        });
    }));

};