// 암호화 해시를 하는 라이브러리 
var bcrypt = require("bcrypt-nodejs");
// node에서 Mongo와 통신하는데 지원되는 라이브러리 
var mongoose = require("mongoose");

// 숫자가 높을 수록 암호화 난이도가 높아짐
var SALT_FACTOR = 10;

// 사용자 database schema (class, prototype) 생성
var userSchema = mongoose.Schema({
    username: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    createdAt: { type: Date, default: Date.now },
    displayName: String,
    bio: String
});
// bcrypt module과 함께 사용하기 위한 아무것도 하지 않는 함수
var noop = function() {};

// 모델이 저장되기 전에 실행되는 함수를 정의
userSchema.pre("save", function(done) {
    var user = this; // 사용자에 대한 참조를 저장
    // 비번이 수정되지 않았다면 건너뛴다
    if (!user.isModified("password")) {
        return done();
    }

    // 비번 해시...  해시에 대한 부가적인 값(salt)를 생성하고 완료되면 내부함수를 호출
    bcrypt.genSalt(SALT_FACTOR, function(err, salt) {
        if (err) { return done(err); }
        bcrypt.hash(user.password, salt, noop, function(err, hashedPassword) {
            if (err) { return done(err); }
            user.password = hashedPassword; // 비번 저장
            done();
        });
    });
});
//사용자의 비번을 검사한다
userSchema.methods.checkPassword = function(guess, done) {
    bcrypt.compare(guess, this.password, function(err, isMatch) {
        done(err, isMatch);
    });
};

// 사용자 이름을 가져오는 메서드 ; displayname 또는 username을 반환한다
userSchema.methods.name = function() {
    return this.displayName || this.username;
};

// 사용자 모델을 user 변수로 만들고, user를 내 보냄  
var User = mongoose.model("User", userSchema);

module.exports = User;