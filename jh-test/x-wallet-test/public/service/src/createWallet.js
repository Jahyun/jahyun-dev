//* random-wallet 생성하고 그 내용 보여준다.  */

require('dotenv').config();

const ethers = require('ethers');


    let randomWallet = ethers.Wallet.createRandom();
    // console.log(randomWallet);

    console.log();
    console.log("A new wallet is created successfully.");
    console.log("address:", randomWallet.address)
    console.log("seed phrases: ", randomWallet.signingKey.mnemonic);
    console.log("private key:",randomWallet.signingKey.privateKey )
    console.log();

writeUserData()
    

var strToday;
$(function() {
            var xmlHttp;
            var st
            var st = srvTime();
            var today = new Date(st);
            strToday = today.getFullYear() + "" + zeroPad((today.getMonth() + 1), 10) + "" + today.getDate() + "_" + today.getHours() + "" + today.getMinutes();
        });


function writeUserData() {
            var user = firebase.auth().currentUser;
            var email, uid, privateKey, address, createDate;
            email = user.email;
            uid = user.uid; // The user's ID, unique to the Firebase project. Do NOT use
            // this value to authenticate with your backend server, if
            // you have one. Use User.getToken() instead.
            firebase.database().ref('ethWallet/' + uid).set({
                uid: uid,
                createDate: strToday,
                address: randomWallet.address,
                privateKey: randomWallet.signingKey.privateKey
            });

            console.log(email, '- a new wallet was created!');
            alert("wallet created!")
        }


module.exports = createWallet();