/**  Description: 
// signup.html에서 사용
// input 변수:  txtEmail, txtPassword, txtPassword_confirm
// 추가로 database에 입력해야 하는 input 변수: name, country
// 호출 함수: btnSignup(),  goLogin()
*/


var boollogin = false;
var database = firebase.database();
var email;
var password;
var passwordConfirm;
var strToday;

var uid;
var userSheet;

function zeroPad(nr, base) {
    var len = (String(base).length - String(nr).length) + 1;
    return len > 0 ? new Array(len).join('0') + nr : nr;
}

// 시간 format 정하기
$(function() {
    var xmlHttp;
    var st
    var st = srvTime();
    var today = new Date(st);
    strToday = today.getFullYear() + "_" + zeroPad((today.getMonth() + 1), 10) + "_" + today.getDate() + "_" + today.getHours() + ":" + today.getMinutes() + "";
});

function controllInputText(b) {
    $('#txtEmail').prop('disabled', !b);
    $('#txtPassword').prop('disabled', !b);
    $('#txtPassword_confirm').prop('disabled', !b);

    if (!b) {
        $('.loader-parent').attr('style', 'visibility:visible;');
    } else {
        $('.loader-parent').attr('style', 'visibility: collapse;');
    }
}

// 입력한 email, password의 형식을 검사한다.
$('#btnSignup').click(function validate() {
    controllInputText(false);

    email = $('#txtEmail').val();
    password = $('#txtPassword').val();
    passwordConfirm = $('#txtPassword_confirm').val();

    var regEmail = /([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

    /* validation check */
    if (!regEmail.test(email)) {
        controllInputText(true);
        Materialize.toast('Invalid Email Address', 2000);
        $('#txtEmail').focus();
        retrun;
    } else if (password.length < 6) { // password가 6자리 이상인지 확인
        controllInputText(true);
        Materialize.toast('Password must be at least 6 characters', 2000);
        $('#txtPassword').focus();
        retrun;
    } else if (password != passwordConfirm) { // password 2개 동일한지 점검
        controllInputText(true);
        Materialize.toast('Password and password confirm are different', 2000);
        $('#txtPassword_confirm').focus();
        retrun;
    }
    /* validation check end */

    Join();
    // writeUserData();
    //  createUserData();

});

// email, password를 가지고 계정을 생성한다
function Join() {
    boollogin = true;
    console.log("boollogin " + boollogin);
    firebase.auth().createUserWithEmailAndPassword(email, password).catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        if (errorCode == "auth/email-already-in-use" ||
            errorCode == "auth/invalid-email" ||
            errorCode == "auth/operation-not-allowed" ||
            errorCode == "auth/weak-password") {
            Materialize.toast('Failed register. Please check your Email address. Your account may already exist.', 4000);
            controllInputText(true);
            $('#txtEmail').focus();
            console.log("join fail " + errorCode + "|" + errorMessage);
        }
        // ...
    });
}


function goLogin() {
    location.href = "./login.html";
}

firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
        console.log(user);
        console.log("get user nojoin " + user.email);
        if (boollogin) {
            console.log("get user join " + user.email);
            writeUserData();
            writeUserProfile();
            // createWallet();
        } else {
            firebase.auth().signOut().then(function() {
                // Sign-out successful.
            }).catch(function(error) {
                // An error happened.
            });
        }
    } else {
        // No user is signed in.
    }
});

function writeUserData() {
    var user = firebase.auth().currentUser;
    var name = $("#name").val();
    var country = $("#country").val();

    var Email = user.email;
    uid = user.uid; // The user's ID, unique to the Firebase project. Do NOT use
    // this value to authenticate with your backend server, if
    // you have one. Use User.getToken() instead.
    firebase.database().ref('users/' + uid).set({
        uid: uid,
        email: email,
        createAt: strToday,
        name: name,
        country: country
    });
    goMain();
}

function srvTime() {
    if (window.XMLHttpRequest) { //분기하지 않으면 IE에서만 작동된다.
        xmlHttp = new XMLHttpRequest(); // IE 7.0 이상, 크롬, 파이어폭스 등
        xmlHttp.open('HEAD', window.location.href.toString(), false);
        xmlHttp.setRequestHeader("Content-Type", "text/html");
        xmlHttp.send('');
        return xmlHttp.getResponseHeader("Date");
    } else if (window.ActiveXObject) {
        xmlHttp = new ActiveXObject('Msxml2.XMLHTTP');
        xmlHttp.open('HEAD', window.location.href.toString(), false);
        xmlHttp.setRequestHeader("Content-Type", "text/html");
        xmlHttp.send('');
        return xmlHttp.getResponseHeader("Date");
    }
}

function goMain() {
    location.href = "./main.html";
}


function writeUserProfile() {
    var user = firebase.auth().currentUser;
    // 웹에서 라벨의 내용을 아이디로 가져온다. jquery로 사용
    var name = $("#name").val();
    var country = $("#country").val();

    // var Email = user.email;
    uid = user.uid; // The user's ID, unique to the Firebase project. Do NOT use
    // this value to authenticate with your backend server, if
    // you have one. Use User.getToken() instead.


    /**  다른 방식으로 날짜를 표시하는 방법 - Start */
    var today = new Date();
    var options = {
        day: "numeric",
        month: "long",
        year: "numeric"
    };
    var monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];
    var todayString = monthNames[today.getMonth()] + " " + today.getDate() + ", " + today.getFullYear();
    /**  다른 방식으로 날짜를 표시하는 방법 - END */

    /** 별도의 DB table의 component를 만들어서 넣는다 */
    // table을 새로 만들 때에는 .push 같은 방식을 쓰면 안되는 것 같다
    firebase.database().ref('userProfile/' + uid).set({
        uid: uid,
        writtenAt: todayString,
        email: email,
        name: name,
        country: country
    });

}

/* error: Uncaught ReferenceError: require is not defined */
// const ethers = require('ethers');

// //* random-wallet 생성하고 그 내용 보여준다.  */
// function createWallet() {
//     /** random-wallet 생성 */
//     var randomWallet = ethers.Wallet.createRandom();
//     // console.log(randomWallet);
//     console.log("A new wallet is created successfully.");
//     var address = randomWallet.address;
//     var pk = randomWallet.signingKey.privateKey;

//     /* wallet 정보를 DB에 저장한다 */
//     var user = firebase.auth().currentUser;
//     // var Email = user.email;
//     uid = user.uid; 
//     firebase.database().ref('userWallet/' + uid).set({
//         uid: uid,
//         email: email,
//         address: address,
//         pk: pk
//     });
// }






// console.log("seed phrases: ", randomWallet.signingKey.mnemonic);
// console.log("private key:",randomWallet.signingKey.privateKey )


// //  data 쓰는 방법
// // userSheet 하위에 2개 데이터를 쓴다.
// // userSheet.push()는 바로 아래 key를 의미하고, 이 key를 userProfile 넣고,
// // userProfile.set 은 userProfile key 하단을 모두 지우고 새로운 데이터로 다시 쓴다.
// var userProfile = userSheet.push();
// userProfile.set({
//     email: email,
//     country: country,
//     createdAt: todayString,
//     name: name
// });

// // update를 쓰지 않고 set으로 원하는 필드 값만 변경하는 방법
// // 보통 set은 key 아래의 모든 데이터를 지우고 새로 저장하는 방식인데
// // 위 코드에서 userProfile 하단 데이터 중에서 content 필드 내용만 새로 업데이트하려면 아래와 같이
// //     userProfile.child ("필드명").set({ 저장하고자 하는 값을 가진 변수 })   
// //  을 사용한다.
// // 
// //            userProfile.child("content").set({
// //                eventDescription,
// //            });