// (function() {

const auth = firebase.auth();

// get elements
const txtEmail = document.getElementById('txtEmail');
const txtPassword = document.getElementById('txtPassword');
const btnLogin = document.getElementById('btnLogin');
const btnSignUp = document.getElementById('btnSignUp');
const btnLogout = document.getElementById('btnLogout');

//add login event
// var el = document.getElementById('btnLogin');
// if (el) {
//     el.addEventListener('click', e => {

btnLogin.addEventListener('click', e => {
    //get email and pw
    const email = txtEmail.value;
    const pass = txtPassword.value;
    const auth = firebase.auth();


    const promise = auth.signInWithEmailAndPassword(email, pass);


    console.log(promise.value);
    promise.catch(e => console.log(e.message));
    console.log('Logged in!', email);
    console.log('< 1 >');
    // window.open('./index.html', '_self', false);
});

// firebase.auth().onAuthStateChanged(function(user) {
//     if (user) {
//         // User is signed in.
//         window.open('./profile.html', '_self', false);
//     } else {
//         // No user is signed in.
//         window.open('./login.html', '_self', false);
//     };
// });

btnSignUp.addEventListener('click', e => {
    //get email and pw
    const email = txtEmail.value;
    const pass = txtPassword.value;
    const auth = firebase.auth();
    //sign up a new account 
    const promise = auth.createUserWithEmailAndPassword(email, pass);
    // 인증 관련 event가 catch되면, 그 event의 메시지를 출력한다
    promise.catch(e => console.log(e.message));
    console.log('New Account created!', email);

});

btnLogout.addEventListener('click', e => {
    // firebase.auth().signOut();
    const promise = auth.signOut();
    console.log('Logged out!', email);

    window.open('./login.html', '_self', false);

});

// add a realtime listerner
// firebase.auth().onAuthStateChanged(firebaseUser => {
//     if (firebaseUser) {
//         console.log(firebaseUser);
//         btnLogout.classList.remove('hide');
//     } else {
//         console.log('not logged in');
//         btnLogout.classList.add('hide');

//     }
// });




// login page와 index 페이지 중에 어디로 갈 것인가를 결정
// $(document).ready(function($) {
//     firebase.auth().onAuthStateChanged(function(user) {

//         // 현재 location의 url에 login 이름을 포함된 경우라면...
//         // var cu = window.location.href;
//         // var n1 = cu.indexOf('login');

//         // user로 인증되면 index.htm로... 아니면 다시 그대로
//         if (user) {

//             console.log('login success')
//             window.open('./index.html', '_self', false);

//         } else {

//             console.log('not authorized')
//             window.open('./login.html', '_self', false);

//         }

//     });
// });


// function logout() {
//     firebase.auth().signOut().then(function() {
//         // Sign-out successful.
//         console.log('Sign-out successful');
//     }).catch(function(error) {
//         // An error happened.
//         console.log('An error happened');
//     });
// }


// function Signin() {

//     var email = document.getElementById('email').value;
//     var password = document.getElementById('password').value;

//     //   firebase.auth().createUserWithEmailAndPassword($("#email").val(), $("#password").val()).then(function(result) {
//     firebase.auth().createUserWithEmailAndPassword(email, password).then(function(result) {

//     }).catch(function(error) {
//         // Handle Errors here.
//         var errorCode = error.code;
//         // var errorMessage = error.message;
//         alert(errorCode);
//         // ...
//     });
// }

// function toggleSignIn() {
//     if (firebase.auth().currentUser) {
//         // [START signout]
//         firebase.auth().signOut();
//         // [END signout]
//     } else {
//         var email = document.getElementById('email').value;
//         var password = document.getElementById('password').value;
//         if (email.length < 4) {
//             alert('Please enter an email address.');
//             return;
//         }
//         if (password.length < 4) {
//             alert('Please enter a password.');
//             return;
//         }
//         // Sign in with email and pass.
//         // [START authwithemail]
//         firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
//             // Handle Errors here.
//             var errorCode = error.code;
//             var errorMessage = error.message;
//             // [START_EXCLUDE]
//             if (errorCode === 'auth/wrong-password') {
//                 alert('Wrong password.');
//             } else {
//                 alert(errorMessage);
//             }
//             console.log(error);
//             document.getElementById('quickstart-sign-in').disabled = false;
//             // [END_EXCLUDE]
//         });
//         // [END authwithemail]
//     }
//     document.getElementById('quickstart-sign-in').disabled = true;
// }


// }());